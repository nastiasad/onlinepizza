<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 27.02.2016
  Time: 13:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.free-couriers" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:choose>
            <c:when test="${noFreeCouriers == null}">
                <h1><fmt:message key="title.free-couriers" bundle="${rb}"/></h1>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="thead.name" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.surname" bundle="${rb}"/></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${freeCouriers}" var="courier">
                        <tr>
                            <td><c:out value="${courier.name}"/></td>
                            <td><c:out value="${courier.surname}"/></td>
                            <td>
                                <form method="post" action="controller" id="chooseCourierForm">
                                    <input type="hidden" name="command" value="admin-choose-courier">
                                    <input type="hidden" name="orderId" value="${orderId}">
                                    <input type="hidden" name="courierId" id="courierId">
                                    <input type="submit" class="custom-btn" value="<fmt:message key="button.choose-courier" bundle="${rb}"/>"
                                           form="chooseCourierForm" onclick='document.getElementById("courierId").value="${courier.id}"'>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                        ${noFreeCouriers}
                </div>
            </c:otherwise>
        </c:choose>

    </div>
</div>

</body>
</html>
