<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 11.02.2016
  Time: 8:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.dishes" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:if test="${dbError != null}">
            <div class="alert alert-danger">
                    ${dbError}
            </div>
        </c:if>
        <c:if test="${cantDeleteDishMessage != null}">
            <div class="alert alert-danger">
                    ${cantDeleteDishMessage}
            </div>
        </c:if>
        <form method="get" action="controller">
            <input type="hidden" name="command" value="admin-add-dish-redirect">
            <input type="submit" class="custom-btn" value="<fmt:message key="button.new-dish" bundle="${rb}"/>">
        </form>
        <c:choose>
            <c:when test="${allDishesError != null}">
                <div class="alert alert-danger">
                        ${allDishesError}
                </div>
            </c:when>
            <c:otherwise>
                <h1><fmt:message key="label.${allDishes.get(0).getType().toString().toLowerCase()}" bundle="${rb}"/></h1>
                <ctg:table-dishes dishes="${allDishes}" userStatus="${status}"/>
            </c:otherwise>
        </c:choose>
    </div>
</div>

</body>
</html>
