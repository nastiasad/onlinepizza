<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 18.02.2016
  Time: 12:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.orders" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>
<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:choose>
            <c:when test="${not empty newOrders}">
                <h1><fmt:message key="title.orders" bundle="${rb}"/></h1>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="thead.order-number" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.date-time" bundle="${rb}"/></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${newOrders}" var="order">
                        <tr>
                            <td><c:out value="${order.id}"/></td>
                            <td><c:out value="${order.getDateTime().getTime()}"/></td>
                            <td>
                                <form method="get" action="controller" id="findCouriersForm">
                                    <input type="hidden" name="command" value="admin-find-couriers">
                                    <input type="hidden" name="orderId" id="orderId">
                                    <input type="submit" class="custom-btn" value="<fmt:message key="button.find-couriers" bundle="${rb}"/>" form="findCouriersForm"
                                           onclick='document.getElementById("orderId").value="${order.id}"'>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger" align="center">
                        ${orderError}
                </div>
            </c:otherwise>
        </c:choose>

    </div>
    </div>
</body>
</html>
