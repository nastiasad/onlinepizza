<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 23.02.2016
  Time: 7:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.edit-dish" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>
<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:if test="${cantFireError != null}">
            <div class="alert alert-danger">
                    ${cantFireError}
            </div>
        </c:if>
        <c:if test="${fireError != null}">
            <div class="alert alert-danger">
                    ${fireError}
            </div>
        </c:if>
        <form class="new-courier-form" method="get" action="controller">
            <input type="hidden" name="command" value="admin-create-courier-redirect">
            <input type="submit" class="custom-btn" value="<fmt:message key="button.new-courier" bundle="${rb}"/>">
        </form>
        <c:choose>
            <c:when test="${not empty couriers}">
                <h1><fmt:message key="title.couriers" bundle="${rb}"/></h1>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="thead.login" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.name" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.surname" bundle="${rb}"/></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${couriers}" var="courier">
                        <tr>
                            <td><c:out value="${courier.login}"/></td>
                            <td><c:out value="${courier.name}"/></td>
                            <td><c:out value="${courier.surname}"/></td>
                            <td>
                                <form method="post" action="controller" id="deleteCourierForm">
                                    <input type="hidden" name="command" value="admin-delete-courier">
                                    <input type="hidden" name="courierId" id="courierId">
                                    <input type="submit" class="custom-btn" value="<fmt:message key="button.fire-courier" bundle="${rb}"/>"
                                           form="deleteCourierForm" onclick='document.getElementById("courierId").value="${courier.id}"'>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                        ${couriersRedirectError}
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>

</body>
</html>
