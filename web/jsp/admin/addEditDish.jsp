<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 11.02.2016
  Time: 8:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title>
        <c:choose>
            <c:when test="${editedDish != null}">
                <fmt:message key="title.edit-dish" bundle="${rb}"/>
            </c:when>
            <c:otherwise>
                <fmt:message key="title.add-dish" bundle="${rb}"/>
            </c:otherwise>
        </c:choose>
    </title>

    <c:import url="/jsp/common/links.jsp"/>

    <script type="text/javascript" src="javascript/jquery-2.1.0.min.js"></script>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc2/css/bootstrap-glyphicons.css">
    <script src="../../js/multiFields.js"></script>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>

<div class="container custom-header main-header">
    <div class="container">
        <div class="card card-container">
            <form class="input-form" method="post" action="controller">
                <input type="hidden" name="command" value="admin-add-edit-dish">
                <input type="hidden" name="editedDishId" value="${editedDish.id}">
                <input type="text" name="name" pattern="^.{3,30}$" value="${editedDish.name}" class="form-control card-input"
                       placeholder="<fmt:message key="label.name" bundle="${rb}"/>" required autofocus>
                <select size="1" name="type" class="form-control card-input">
                    <c:forEach items="${dishTypes}" var="type">
                        <option
                                <c:if test="${editedDish.type == type}">
                                selected="selected"
                                </c:if>
                                value="${type.toString()}"><fmt:message key="label.${type.toString().toLowerCase()}" bundle="${rb}"/></option>
                    </c:forEach>
                </select>

                <input type="text" name="description" value="${editedDish.description}" class="form-control card-input"
                       placeholder="<fmt:message key="label.description" bundle="${rb}"/>">
                <div id="ingredients" class="multi-field-wrapper">
                    <div class="multi-fields">
                        <c:choose>
                            <c:when test="${editedDish == null}">
                                <div class="multi-field">
                                    <div class="row" >
                                        <div class="col-md-9">
                                            <input type="text" name="ingredient0" id="ingredient0"
                                                   class="form-control card-input" placeholder="<fmt:message key="label.ingredient" bundle="${rb}"/>" required>
                                        </div>
                                        <div class="col-md-3 custom-col">
                                            <button type="button" class="remove-field custom-btn remove-btn">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </c:when>

                            <c:when test="${editedDish != null}">
                                <c:forEach var="i" begin="0" end="${editedDish.getIngredients().size()-1}">
                                    <div class="multi-field">
                                        <div class="row" >
                                            <div class="col-md-9">
                                                <input type="text" name="ingredient${i}" id="ingredient${i}"
                                                       value="${editedDish.getIngredients().get(i)}"
                                                       class="form-control card-input">
                                            </div>
                                            <div class="col-md-3 custom-col">
                                                <button type="button" class="remove-field custom-btn remove-btn">
                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </div>
                    <button type="button" class="add-field custom-btn"><fmt:message key="button.add-field" bundle="${rb}"/></button>
                </div>
                <input type="number" min="1" name="weight" value="${editedDish.weight}" class="form-control card-input"
                       placeholder="<fmt:message key="label.weight" bundle="${rb}"/>" required>
                <input type="number" min="0" name="cost" value="${editedDish.cost}" class="form-control card-input"
                       placeholder="<fmt:message key="label.cost" bundle="${rb}"/>" required>
                <c:choose>
                    <c:when test="${editedDish != null}">
                        <input type="submit" class="custom-btn" value="<fmt:message key="button.edit" bundle="${rb}"/>">
                    </c:when>
                    <c:otherwise>
                        <input type="submit" class="custom-btn" value="<fmt:message key="button.add" bundle="${rb}"/>">
                    </c:otherwise>
                </c:choose>
            </form>

            ${errorAddEditDish}
        </div>
    </div>
</div>
</body>
</html>


