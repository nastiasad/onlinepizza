<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 27.02.2016
  Time: 12:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.orders" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:choose>
            <c:when test="${courierOrdersError == null}">
                <c:forEach items="${courierOrders}" var="order">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><c:out value="${order.getAddress().city} city, ${order.getAddress().street} street,
                                    ${order.getAddress().house} - ${order.getAddress().flat}"/></h2>
                            <h2>Dishes</h2>
                            <c:forEach items="${order.getOrder().getDishes()}" var="dish">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><c:out value="${dish.name}"/></h4>
                                        <h4><c:out value="Cost: ${dish.cost}"/></h4>
                                        <h4><c:out value="Amount: ${dish.amount}"/></h4>
                                    </div>
                                </div>
                            </c:forEach>
                            <h2><c:out value="TOTAL: ${order.total}"/></h2>
                            <form method="post" action="controller" id="doneOrderForm">
                                <input type="hidden" name="command" value="courier-done-order">
                                <input type="hidden" name="orderId" id="orderId">
                                <input type="submit" class="custom-btn" value="<fmt:message key="button.done" bundle="${rb}"/>" form="doneOrderForm"
                                       onclick='document.getElementById("orderId").value="${order.getOrder().id}"'>
                            </form>
                        </div>
                    </div>
                </c:forEach>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                        ${courierOrdersError}
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>


</body>
</html>
