<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 20.02.2016
  Time: 9:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ctg" uri="customtags"%><html>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav custom-navbar">
                <li>
                    <form class="navbar-form" id="mainForm" method="get" action="controller">
                        <input type="hidden" name="command" value="main-redirect"/>
                        <a href="#" onclick="document.getElementById('mainForm').submit()">
                            <fmt:message key="button.main" bundle="${rb}"/>
                        </a>
                    </form>
                </li>
                <c:choose>
                    <c:when test="${status == null || status == 'GUEST' || status == 'CLIENT'}">
                        <ctg:dropdown-dish nameParam="button.menu" userStatus="client"/>
                        <c:if test="${status == 'CLIENT'}">
                            <li>
                                <form class="navbar-form" id="curOrderForm" method="get" action="controller">
                                    <input type="hidden" name="command" value="client-cur-order-redirect"/>
                                    <a href="#" onclick="document.getElementById('curOrderForm').submit()">
                                        <fmt:message key="button.my-order" bundle="${rb}"/>
                                    </a>
                                </form>
                            </li>
                            <li>
                                <form class="navbar-form" id="allOrdersForm" method="get" action="controller">
                                    <input type="hidden" name="command" value="client-all-orders-redirect"/>
                                    <a href="#" onclick="document.getElementById('allOrdersForm').submit()">
                                        <fmt:message key="button.all-orders" bundle="${rb}"/>
                                    </a>
                                </form>
                            </li>
                        </c:if>
                    </c:when>
                    <c:when test="${status == 'ADMIN'}">
                        <ctg:dropdown-dish nameParam="button.dishes" userStatus="admin"/>
                        <li>
                            <form class="navbar-form" id="ordersForm" method="get" action="controller">
                                <input type="hidden" name="command" value="admin-orders-redirect"/>
                                <a href="#" onclick="document.getElementById('ordersForm').submit()">
                                    <fmt:message key="button.orders" bundle="${rb}"/>
                                </a>
                            </form>
                        </li>
                        <li>
                            <form class="navbar-form" id="couriersForm" method="get" action="controller">
                                <input type="hidden" name="command" value="admin-couriers-redirect"/>
                                <a href="#" onclick="document.getElementById('couriersForm').submit()">
                                    <fmt:message key="button.couriers" bundle="${rb}"/>
                                </a>
                            </form>
                        </li>
                    </c:when>
                    <c:when test="${status == 'COURIER'}">
                        <li>
                            <form class="navbar-form" id="courierOrdersForm" method="get" action="controller">
                                <input type="hidden" name="command" value="courier-orders-redirect"/>
                                <a href="#" onclick="document.getElementById('courierOrdersForm').submit()">
                                    <fmt:message key="button.orders" bundle="${rb}"/>
                                </a>
                            </form>
                        </li>
                    </c:when>
                </c:choose>

            </ul>

            <ul class="nav navbar-nav navbar-right custom-navbar">
                <li>
                    <form class="navbar-form" id="engForm" method="POST" action="controller">
                        <input type="hidden" name="command" value="change-locale"/>
                        <input type="hidden" name="locale" value="eng">
                        <a href="#" onclick="document.getElementById('engForm').submit()">
                            ENG
                        </a>
                    </form>
                </li>
                <li>
                    <form class="navbar-form" id="ruForm" method="POST" action="controller">
                        <input type="hidden" name="command" value="change-locale"/>
                        <input type="hidden" name="locale" value="ru">
                        <a href="#" onclick="document.getElementById('ruForm').submit()">
                            РУ
                        </a>
                    </form>
                </li>
                <c:choose>
                    <c:when test="${status == null || status == 'GUEST'}">
                        <li>
                            <form class="navbar-form" id="signupForm" method="get" action="controller">
                                <input type="hidden" name="command" value="signup-redirect">
                                <a href="#" onclick="document.getElementById('signupForm').submit()">
                                    <fmt:message key="button.sign-up" bundle="${rb}"/>
                                </a>
                            </form>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>
                                <fmt:message key="button.log-in" bundle="${rb}"/>
                            </b> <span class="caret"></span></a>
                            <ul id="login-dp" class="dropdown-menu pull-right">
                                <li>
                                    <c:import url="../common/login.jsp"/>
                                </li>
                            </ul>
                        </li>
                    </c:when>
                    <c:when test="${status == 'CLIENT' || status == 'ADMIN' || status == 'COURIER'}">
                        <li>
                            <form class="navbar-form" id="logoutForm" method="post" action="controller">
                                <input type="hidden" name="command" value="logout">
                                <a href="#" onclick="document.getElementById('logoutForm').submit()">
                                    <fmt:message key="button.log-out" bundle="${rb}"/>
                                </a>
                            </form>
                        </li>
                    </c:when>
                </c:choose>

            </ul>
        </div>
    </div>
</nav>
