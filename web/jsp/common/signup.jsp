<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 03.02.2016
  Time: 17:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.sign-up" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script>
        function checkPasswordMatch() {
            var password = $("#txtPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();

            if (password != confirmPassword || password == "") {
                $("#divCheckPasswordMatch").html('<fmt:message key="message.not-match" bundle="${rb}"/>');
                $(":submit").attr('disabled', 'true');
            }
            else {
                $("#divCheckPasswordMatch").html('<fmt:message key="message.match" bundle="${rb}"/>');
                $(":submit").removeAttr("disabled");
            }
        }
    </script>
</head>
<body>
<c:import url="../common/header.jsp"/>
<div class="container custom-header main-header">
    <div class="container">
        <div class="card-container">
            <form id="signupForm" name="signupForm" class="input-form required-fields" method="POST" action="controller">
                <input type="hidden" name="command" value="signup" />
                <input type="text" pattern="^.{3,30}$" name="name" class="form-control card-input" placeholder="<fmt:message key="label.person-name" bundle="${rb}"/>" required autofocus>
                <input type="text" pattern="^.{3,30}$" name="surname" class="form-control card-input" placeholder="<fmt:message key="label.surname" bundle="${rb}"/>" required>
                <input type="email" pattern="^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"
                       name="mail" class="form-control card-input"  placeholder="<fmt:message key="label.mail" bundle="${rb}"/>" required>
                <input type="text" pattern="^[a-zA-Z0-9_]{4,30}$" name="login" class="form-control card-input"  placeholder="<fmt:message key="label.login" bundle="${rb}"/>" required>
                <input type="password" pattern="^.{4,30}$" id="txtPassword" name="password" class="form-control card-input" placeholder="<fmt:message key="label.password" bundle="${rb}"/>" required>
                <input type="password" pattern="^.{4,30}$" id="txtConfirmPassword" onkeyup="checkPasswordMatch()" name="confirm" class="form-control card-input" placeholder="<fmt:message key="label.confirm" bundle="${rb}"/>" required>

                <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>

                <c:choose>
                    <c:when test="${createCourier == true}">
                        <input type="hidden" name="create-client" value="true">
                        <input type="submit" disabled="true" class="custom-btn" value="<fmt:message key="button.sign-up-courier" bundle="${rb}"/>">
                    </c:when>
                    <c:otherwise>
                        <input type="submit" disabled="true" class="custom-btn" value="<fmt:message key="button.sign-up" bundle="${rb}"/>">
                    </c:otherwise>
                </c:choose>
                ${errorSignupMessage}
            </form>
        </div>
    </div>
</div>
</body>
</html>
