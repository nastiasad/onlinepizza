<%--
  Created by IntelliJ IDEA.
  User: anastasiya
  Date: 30.03.16
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.main" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container ">
    <div class="dishes-context">
        <div class="alert alert-danger">
            ${errorText}
        </div>
    </div>
</div>

</body>
</html>
