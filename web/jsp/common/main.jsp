<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 03.02.2016
  Time: 12:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.main" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header main-header">
    <div class="header-content">
        <h2 class="this-is-header"><b><fmt:message key="title.this-is" bundle="${rb}"/></b></h2>
        <h1 class="title-header"><img src="../../img/left-symbol.png"><fmt:message key="title.your-pizza" bundle="${rb}"/><img src="../../img/right-symbol.png"></h1>
    </div>
</header>

<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>


