<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 03.02.2016
  Time: 17:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>

<div class="login-form-container">
<div class="row">
    <div class="col-md-12">
        <form class="form" method="post" action="controller">
            <input type="hidden" name="command" value="login">
            <div class="form-group">
                <input type="text" class="form-control card-input" name="login" placeholder="<fmt:message key="label.login" bundle="${rb}"/>" required autofocus>
            </div>
            <div class="form-group">
                <input type="password" class="form-control card-input" name="password" placeholder="<fmt:message key="label.password" bundle="${rb}"/>" required>
            </div>
            <div class="form-group">
                <button type="submit" class="btn custom-btn"><fmt:message key="button.log-in" bundle="${rb}"/></button>
            </div>
        </form>
    </div>
    <div class="bottom text-center">
        <fmt:message key="label.new-here" bundle="${rb}"/>
        <form class="join-us" id="signupForm" method="get" action="controller">
            <input type="hidden" name="command" value="signup-redirect">
            <a class="my-join-class" href="#" onclick="document.getElementById('signupForm').submit()"><b><fmt:message key="button.join-us" bundle="${rb}"/></b></a>
        </form>
    </div>
    <div class="bottom text-left custom-text-left">
        ${errorLoginMessage}
        ${errorUnknownLoginMessage}
    </div>
</div>
</div>



