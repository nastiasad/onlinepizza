<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 16.02.2016
  Time: 8:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.checkout" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>

<div class="container custom-header main-header">
    <div class="container">
        <div class="card card-container">
            <c:if test="${checkoutError != null}">
                <div class="alert alert-danger">
                        ${checkoutError}
                </div>
            </c:if>
            <c:choose>
                <c:when test="${prevAddressesNotFound != null}">
                    <div class="alert alert-danger">
                            ${prevAddressesNotFound}
                    </div>
                </c:when>
                <c:otherwise>
                    <form class="input-form" method="get" action="controller">
                        <input type="hidden" name="command" value="client-prev-addresses">
                        <input type="submit" class="custom-btn" value="<fmt:message key="button.choose-prev" bundle="${rb}"/>">
                    </form>
                </c:otherwise>
            </c:choose>
            <form class="input-form" method="post" action="controller">
                <input type="hidden" name="command" value="client-checkout">
                <input type="text" class="form-control card-input" name="city" value="Minsk" readonly>
                <input type="text" pattern="^.{3,30}$" class="form-control card-input" name="street" value="${addr.street}"
                       placeholder="<fmt:message key="label.street" bundle="${rb}"/>" required autofocus>
                <input type="number" min="1" class="form-control card-input" name="house" value="${addr.house}"
                       placeholder="<fmt:message key="label.house" bundle="${rb}"/>" required>
                <input type="number" min="1" class="form-control card-input" name="flat" value="${addr.flat}"
                       placeholder="<fmt:message key="label.flat" bundle="${rb}"/>" required>
                <input type="submit" class="custom-btn" value="<fmt:message key="button.send-order" bundle="${rb}"/>">
            </form>
        </div>
    </div>
</div>
</body>
</html>
