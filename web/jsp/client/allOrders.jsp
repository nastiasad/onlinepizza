<%--
  Created by IntelliJ IDEA.
  User: anastasiya
  Date: 18.03.16
  Time: 14:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.all-orders" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>

<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container">
    <div class="dishes-context">
        <c:choose>
            <c:when test="${ordersError == null}">
                <h1><fmt:message key="title.all-orders" bundle="${rb}"/></h1>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><fmt:message key="thead.order-number" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.date-time" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.dishes" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.order-status" bundle="${rb}"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${orders}" var="order">
                        <tr>
                            <td><c:out value="${order.id}"/></td>
                            <td><c:out value="${order.getDateTime().getTime()}"/></td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <fmt:message key="label.dishes" bundle="${rb}"/>
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-right custom-dropdown" aria-labelledby="dropdownMenu1">
                                        <c:forEach items="${order.dishes}" var="dish">
                                            <li><c:out value='${dish.name}: ${dish.amount}'/></li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </td>
                            <td><c:out value="${order.status}"/></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                        ${ordersError}
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>


</body>
</html>
