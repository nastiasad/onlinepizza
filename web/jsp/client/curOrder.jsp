<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 24.02.2016
  Time: 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.cur-order" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>
<header class="custom-header dishes-bg-header"></header>

<div class="container dishes-context-container ">
    <div class="dishes-context">
        <c:if test="${deleteOrderDishError != null}">
            <div class="alert alert-danger">
                    ${deleteOrderDishError}
            </div>
        </c:if>
        <c:choose>
            <c:when test="${curOrderError == null}">
                <h1><fmt:message key="title.cur-order" bundle="${rb}"/></h1>
                <table class="table table-hover ">
                    <thead>
                    <tr>
                        <th><fmt:message key="thead.name" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.type" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.number-dishes" bundle="${rb}"/></th>
                        <th><fmt:message key="thead.price" bundle="${rb}"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${tempUserOrder}" var="dish">
                        <tr>
                            <td><c:out value="${dish.name}"/></td>
                            <td><c:out value="${dish.type}"/></td>
                            <td><c:out value="${dish.amount}"/></td>
                            <td><c:out value="${dish.cost}"/></td>
                            <td>
                                <form method="post" action="controller" id="deleteDishForm">
                                    <input type="hidden" name="command" value="client-delete-order-dish">
                                    <input type="hidden" name="dishId" id="dishId">
                                    <input type="submit" class="custom-btn" value="<fmt:message key="button.remove" bundle="${rb}"/>" form="deleteDishForm"
                                           onclick='document.getElementById("dishId").value="${dish.id}"'>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td><fmt:message key="label-total" bundle="${rb}"/></td>
                        <td></td><td></td>
                        <td><c:out value="${orderPrice}"/></td>
                    </tr>
                    </tbody>
                </table>
                <form method="get" action="controller">
                    <input type="hidden" name="command" value="client-checkout-redirect">
                    <input type="submit" class="custom-btn" value="<fmt:message key="button.to-checkout" bundle="${rb}"/>">
                </form>
            </c:when>
            <c:otherwise>
                <div class="alert alert-danger">
                        ${curOrderError}
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
