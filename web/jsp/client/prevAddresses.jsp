<%--
  Created by IntelliJ IDEA.
  User: Анастасия
  Date: 17.02.2016
  Time: 7:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="resources.pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="title.prev-addr" bundle="${rb}"/></title>
    <c:import url="/jsp/common/links.jsp"/>
</head>
<body>
<c:import url="/jsp/common/header.jsp"/>

<div class="container custom-header main-header">
    <div class="container">
        <div class="card card-container">
            <c:if test="${not empty addresses}">
                <c:forEach items="${addresses}" var="address">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post" action="controller" id="chooseAddrForm">
                                <input type="hidden" name="command" value="client-choose-address">
                                <input type="hidden" name="addrId" id="addrId">
                                <p><c:out value="${address.city}, ${address.street}, house ${address.house}, flat ${address.flat}"/></p>
                                <input type="submit" class="custom-btn" value="<fmt:message key="button.choose" bundle="${rb}"/>" form="chooseAddrForm"
                                       onclick='document.getElementById("addrId").value="${address.getId()}"'>
                            </form>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
