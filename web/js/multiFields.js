$( document ).ready(function() {
    $('.multi-field-wrapper').each(function () {
        var $wrapper = $('.multi-fields', this);

        $(".add-field", $(this)).click(function (e) {
            var num = $('.multi-field', $wrapper).length;
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').attr('name', 'ingredient' + num).focus();
        });

        $('.multi-field .remove-field', $wrapper).click(function () {
            var num = $('.multi-field', $wrapper).length;
            if (num > 1) {
                var row = $(this).parent().parent().parent();
                row.remove();
                $('.multi-field').each(function (index, element) {
                    $(element).find('input').attr('name', 'ingredient' + index);
                });
            }
        });
    });
});