package by.sadovaya.pizza.filter;


import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.enums.UserStatus;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by anastasiya on 23.03.16.
 */

@WebFilter( urlPatterns = { "/*" }, initParams = { @WebInitParam(name = "ERROR_PATH", value = "/jsp/common/errorPage.jsp") })
public class AccessRestrictionFilter implements Filter {
    private String errorPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        errorPath = filterConfig.getInitParameter("ERROR_PATH");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        UserStatus status = (UserStatus) httpRequest.getSession().getAttribute("status");

        StringBuffer requestURL = httpRequest.getRequestURL();
        if (httpRequest.getQueryString() != null) {
            requestURL.append("?").append(httpRequest.getQueryString());
        }
        String completeURL = requestURL.toString();
        UserStatus [] statusArray = {UserStatus.ADMIN, UserStatus.COURIER, UserStatus.CLIENT};
        for (UserStatus curStatus: statusArray) {
            Pattern p = Pattern.compile(".+?command=" + curStatus.toString().toLowerCase() + "-.+");
            Matcher m = p.matcher(completeURL);
            if(m.matches() && status != curStatus) {
                request.setAttribute("errorText", PageContentManager.getProperty("message.wrong-access"));
                request.getServletContext().getRequestDispatcher(errorPath).forward(request, response);
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {}
}
