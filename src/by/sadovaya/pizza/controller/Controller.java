package by.sadovaya.pizza.controller;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.CommandFactory;
import by.sadovaya.pizza.enums.UserStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Анастасия on 26.01.2016.
 */

@WebServlet("/controller")
public class Controller extends HttpServlet {
    private static final String CONFIG_PATH = "/config/log4j.xml";
    private static final Logger LOGGER = Logger.getLogger(Controller.class);

    public void init() {
        String realConfiguration = getServletContext().getRealPath(CONFIG_PATH);
        new DOMConfigurator().doConfigure(realConfiguration, LogManager.getLoggerRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("status") == null) {
            request.getSession().setAttribute("status", UserStatus.GUEST);
        }
        Command command = CommandFactory.defineCommand(request.getParameter("command"));
        String page = command.execute(request);

        if (page != null) {
            getServletContext().getRequestDispatcher(page).forward(request, response);
        }
    }
}
