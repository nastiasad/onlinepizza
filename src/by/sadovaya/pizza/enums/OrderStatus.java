package by.sadovaya.pizza.enums;

/**
 * Created by Анастасия on 18.02.2016.
 */
public enum OrderStatus {
    NEW,
    ON_THE_WAY,
    DONE
}
