package by.sadovaya.pizza.enums;

/**
 * Created by Анастасия on 11.02.2016.
 */
public enum DishType {
    PIZZA,
    SIDE,
    SALAD,
    DESSERT
}
