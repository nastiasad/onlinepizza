package by.sadovaya.pizza.enums;

/**
 * Created by Анастасия on 07.02.2016.
 */
public enum UserStatus {
    GUEST,
    ADMIN,
    CLIENT,
    COURIER,
    FIRED_COURIER
}
