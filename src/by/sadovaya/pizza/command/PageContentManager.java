package by.sadovaya.pizza.command;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Анастасия on 07.02.2016.
 */
public class PageContentManager {
    private static ResourceBundle resourceBundle;
    private static Locale locale = new Locale("ru", "RU");

    private PageContentManager() {}

    public static void setLocale (Locale loc) {
        locale = loc;
    }

    public static String getProperty (String key) {
        resourceBundle = ResourceBundle.getBundle("resources.pagecontent", locale);
        return resourceBundle.getString(key);
    }

}
