package by.sadovaya.pizza.command.courier;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.CourierOrder;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.CourierOrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 27.02.2016.
 */
public class CourierOrdersRedirectCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(CourierOrdersRedirectCommand.class);
    private static Command instance = new CourierOrdersRedirectCommand();
    private CourierOrdersRedirectCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        try {
            long courierId = (long) request.getSession().getAttribute("currentUserId");
            List<CourierOrder> courierOrders = CourierOrderServiceImpl.getInstance().getCourierOrders(courierId);
            if (courierOrders.size() > 0) {
                request.setAttribute("courierOrders", courierOrders);
            } else {
                request.setAttribute("courierOrdersError", PageContentManager.getProperty("message.courierOrders"));
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in CourierOrdersRedirectCommand", e);
            request.setAttribute("courierOrdersError", PageContentManager.getProperty("message.courierOrders"));
        }
        return PathManager.getProperty("page.courier.orders");
    }
}
