package by.sadovaya.pizza.command.courier;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.CourierOrder;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.CourierOrderServiceImpl;
import by.sadovaya.pizza.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 01.03.2016.
 */
public class DoneOrderCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(DoneOrderCommand.class);
    private static Command instance = new DoneOrderCommand();
    private DoneOrderCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long orderId = Long.parseLong(request.getParameter("orderId"));
        try {
            long courierId = (long) request.getSession().getAttribute("currentUserId");
            OrderServiceImpl.getInstance().makeOrderDone(orderId, courierId);
            List<CourierOrder> courierOrders = CourierOrderServiceImpl.getInstance().getCourierOrders(courierId);
            if (courierOrders.isEmpty()) {
                request.setAttribute("courierOrdersError", PageContentManager.getProperty("message.courierOrders"));
            } else {
                request.setAttribute("courierOrders", courierOrders);
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in DoneOrderCommand", e);
            request.setAttribute("courierOrdersError", PageContentManager.getProperty("message.courierOrders"));
        }
        return PathManager.getProperty("page.courier.orders");
    }
}
