package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.common.DishesRedirectCommand;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 15.02.2016.
 */
public class DeleteDishCommand implements Command{
    private static final Logger LOGGER = Logger.getLogger(DeleteDishCommand.class);
    private static Command instance = new DeleteDishCommand();
    private DeleteDishCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long dishId = Long.parseLong(request.getParameter("deleteDishId"));
        DishType dishType = DishType.PIZZA;
        try {
            dishType = DishServiceImpl.getInstance().getById(dishId).getType();
            if (DishServiceImpl.getInstance().deleteDish(dishId)) {
                List<Dish> allDishes = DishServiceImpl.getInstance().getByType(dishType);
                request.setAttribute("allDishes", allDishes);
            } else {
                request.setAttribute("cantDeleteDishMessage",
                        PageContentManager.getProperty("message.cantDeleteDish"));
            }

        } catch (ServiceException e) {
            LOGGER.error("Exception in DeleteDishCommand", e);
            request.setAttribute("dbError", PageContentManager.getProperty("message.dbError"));
        }
        request.setAttribute("dishType", dishType);
        return DishesRedirectCommand.getInstance().execute(request);
    }
}
