package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Анастасия on 11.02.2016.
 */
public class AddEditDishCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddEditDishCommand.class);
    private static Command instance = new AddEditDishCommand();
    private AddEditDishCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        String name = request.getParameter("name");
        DishType type = DishType.valueOf(request.getParameter("type").toUpperCase());
        String description = request.getParameter("description");
        ArrayList <String> ingredients = new ArrayList<>();
        int i=0;
        while (request.getParameter("ingredient" + i) != null) {
            String ingred = request.getParameter("ingredient" + i);
            if (!ingred.equals("")) {
                ingredients.add(ingred);
            }
            i++;
        }
        int weight = Integer.parseInt(request.getParameter("weight"));
        int cost = Integer.parseInt(request.getParameter("cost"));

        String id = request.getParameter("editedDishId");
        try {
            if (id.equals("")) {
                DishServiceImpl.getInstance().createDish(name, type, description, ingredients, weight, cost);
            } else {
                long dishId = Long.parseLong(id);
                DishServiceImpl.getInstance().updateDish(name, type, description, ingredients, weight, cost, dishId);
            }
            List<Dish> allDishes = DishServiceImpl.getInstance().getByType(type);
            request.getSession().setAttribute("allDishes", allDishes);
            page = PathManager.getProperty("page.admin.dishes");
        } catch (ServiceException e) {
            LOGGER.error("In AddEditDishCommand", e);
            if (id.equals("")) {
                request.setAttribute("errorAddEditDish", PageContentManager.getProperty("message.addDishError"));
            } else {
                request.setAttribute("errorAddEditDish", PageContentManager.getProperty("message.editDishError"));
                long dishId = Long.parseLong(id);
                try {
                    Dish editedDish = DishServiceImpl.getInstance().getById(dishId);
                    request.setAttribute("editedDish", editedDish);
                } catch (ServiceException e1) {
                    LOGGER.error("In AddEditDishCommand while getting dish by id", e1);
                }
            }
            page = PathManager.getProperty("page.admin.add-edit-dish");
        }
        return page;
    }
}
