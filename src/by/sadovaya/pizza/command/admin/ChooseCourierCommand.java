package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.CourierServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 27.02.2016.
 */
public class ChooseCourierCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(ChooseCourierCommand.class);
    private static Command instance = new ChooseCourierCommand();
    private ChooseCourierCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long courierId = Long.parseLong(request.getParameter("courierId"));
        long orderId = Long.parseLong(request.getParameter("orderId"));
        try {
            CourierServiceImpl.getInstance().setCourierForOrder(orderId, courierId, CourierStatus.BUSY);
        } catch (ServiceException e1) {
            LOGGER.error("Exception in ChooseCourierCommand while setting courier", e1);
            request.setAttribute("orderError", PageContentManager.getProperty("message.setCourierError"));
        }
        return OrdersRedirectCommand.getInstance().execute(request);
    }
}
