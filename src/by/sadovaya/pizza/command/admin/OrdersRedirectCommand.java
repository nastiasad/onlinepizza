package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 18.02.2016.
 */
public class OrdersRedirectCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(OrdersRedirectCommand.class);
    private static Command instance = new OrdersRedirectCommand();
    private OrdersRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {

        try {
            List<Order> newOrders = OrderServiceImpl.getInstance().getAllNew();
            if (newOrders.size() > 0) {
                request.setAttribute("newOrders", newOrders);
            } else {
                request.setAttribute("orderError", PageContentManager.getProperty("message.getOrdersError"));
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in OrdersRedirectCommand", e);
            request.setAttribute("orderError", PageContentManager.getProperty("message.getOrdersError"));
        }
        return PathManager.getProperty("page.admin.orders");
    }
}
