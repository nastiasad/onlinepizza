package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.CourierServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 18.02.2016.
 */
public class FindCouriersCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(FindCouriersCommand.class);
    private static Command instance = new FindCouriersCommand();
    private FindCouriersCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long orderId = Long.parseLong(request.getParameter("orderId"));
        try {
            List<User> freeCouriers = CourierServiceImpl.getInstance().getFreeCouriers();
            if (freeCouriers.size() > 0) {
                request.setAttribute("freeCouriers", freeCouriers);
            } else {
                request.setAttribute("noFreeCouriers", PageContentManager.getProperty("message.noFreeCouriers"));
            }
            request.setAttribute("orderId", orderId);
        } catch (ServiceException e) {
            LOGGER.error("Exception in FindCouriersCommand", e);
            request.setAttribute("noFreeCouriers", PageContentManager.getProperty("message.noFreeCouriers"));
        }
        return PathManager.getProperty("page.admin.free-couriers");

    }
}
