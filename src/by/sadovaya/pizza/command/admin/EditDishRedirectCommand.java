package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 14.02.2016.
 */
public class EditDishRedirectCommand implements Command{
    private static final Logger LOGGER = Logger.getLogger(EditDishRedirectCommand.class);
    private static Command instance = new EditDishRedirectCommand();
    private EditDishRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        request.setAttribute("dishTypes", DishType.values());

        long dishId = Long.parseLong(request.getParameter("dishEditId"));
        try {
            Dish dish = DishServiceImpl.getInstance().getById(dishId);
            request.setAttribute("editedDish", dish);
            page = PathManager.getProperty("page.admin.add-edit-dish");
        } catch (ServiceException e) {
            LOGGER.error("In EditDishRedirectCommand", e);
            request.setAttribute("dbError", PageContentManager.getProperty("message.dbError"));
            page = PathManager.getProperty("page.admin.dishes");
        }
        return page;
    }
}
