package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PathManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 18.02.2016.
 */
public class CreateCourierRedirectCommand implements Command {
    private static Command instance = new CreateCourierRedirectCommand();
    private CreateCourierRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("createCourier", true);
        return PathManager.getProperty("page.signup");
    }
}
