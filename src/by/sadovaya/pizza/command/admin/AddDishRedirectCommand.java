package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.enums.DishType;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by anastasiya on 20.03.16.
 */
public class AddDishRedirectCommand implements Command {
    private static Command instance = new AddDishRedirectCommand();
    private AddDishRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.setAttribute("dishTypes", DishType.values());
        return PathManager.getProperty("page.admin.add-edit-dish");
    }
}
