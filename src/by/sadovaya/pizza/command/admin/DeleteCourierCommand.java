package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 01.03.2016.
 */
public class DeleteCourierCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(DeleteCourierCommand.class);
    private static Command instance = new DeleteCourierCommand();
    private DeleteCourierCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long courierId = Long.parseLong(request.getParameter("courierId"));
        try {
            boolean isFired = UserServiceImpl.getInstance().fireCourier(courierId);
            if (!isFired) {
                request.setAttribute("cantFireError", PageContentManager.getProperty("message.cantFireCourier"));
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in DeleteCourierCommand", e);
            request.setAttribute("fireError", PageContentManager.getProperty("message.fireError"));
        }
        return CouriersRedirectCommand.getInstance().execute(request);
    }
}
