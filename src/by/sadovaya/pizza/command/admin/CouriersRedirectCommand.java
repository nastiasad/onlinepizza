package by.sadovaya.pizza.command.admin;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 23.02.2016.
 */
public class CouriersRedirectCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(CouriersRedirectCommand.class);
    private static Command instance = new CouriersRedirectCommand();
    private CouriersRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        try {
            List<User> couriers = UserServiceImpl.getInstance().getAllCouriers();
            if (couriers.size() > 0) {
                request.setAttribute("couriers", couriers);
            } else {
                request.setAttribute("couriersRedirectError",
                        PageContentManager.getProperty("message.couriersRedirectError"));
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in CouriersRedirectCommand", e);
            request.setAttribute("couriersRedirectError",
                    PageContentManager.getProperty("message.couriersRedirectError"));
        }
        return PathManager.getProperty("page.admin.couriers");
    }
}
