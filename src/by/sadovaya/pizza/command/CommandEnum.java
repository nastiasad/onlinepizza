package by.sadovaya.pizza.command;

import by.sadovaya.pizza.command.admin.*;
import by.sadovaya.pizza.command.client.*;
import by.sadovaya.pizza.command.common.*;
import by.sadovaya.pizza.command.courier.CourierOrdersRedirectCommand;
import by.sadovaya.pizza.command.courier.DoneOrderCommand;

/**
 * Created by Анастасия on 03.02.2016.
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = LoginCommand.getInstance();
        }
    },
    SIGNUP_REDIRECT {
        {
            this.command = new RedirectCommand("page.signup");
        }
    },
    SIGNUP {
        {
            this.command = SignupCommand.getInstance();
        }
    },
    LOGOUT {
        {
            this.command = LogoutCommand.getInstance();
        }
    },
    MAIN_REDIRECT {
        {
            this.command = new RedirectCommand("page.main");
        }
    },
    DISHES_REDIRECT {
        {
            this.command = DishesRedirectCommand.getInstance();
        }
    },
    CHANGE_LOCALE {
        {
            this.command = ChangeLocaleCommand.getInstance();
        }
    },





    CLIENT_ADD_TO_ORDER {
        {
            this.command = AddToOrderCommand.getInstance();
        }
    },

    CLIENT_CHECKOUT_REDIRECT {
        {
            this.command =  new RedirectCommand("page.client.checkout");
        }
    },
    CLIENT_CHECKOUT {
        {
            this.command = CheckoutCommand.getInstance();
        }
    },
    CLIENT_PREV_ADDRESSES {
        {
            this.command = ShowPrevAddressesCommand.getInstance();
        }
    },
    CLIENT_CHOOSE_ADDRESS {
        {
            this.command = ChooseAddressCommand.getInstance();
        }
    },
    CLIENT_CUR_ORDER_REDIRECT {
        {
            this.command = CurOrderRedirectCommand.getInstance();
        }
    },
    CLIENT_DELETE_ORDER_DISH {
        {
            this.command = DeleteOrderDishCommand.getInstance();
        }
    },
    CLIENT_ALL_ORDERS_REDIRECT {
        {
            this.command = AllOrdersRedirectCommand.getInstance();
        }
    },






    ADMIN_DISHES_REDIRECT {
        {
            this.command = DishesRedirectCommand.getInstance();
        }
    },
    ADMIN_ADD_DISH_REDIRECT {
        {
            this.command = AddDishRedirectCommand.getInstance();
        }
    },
    ADMIN_ADD_EDIT_DISH {
        {
            this.command = AddEditDishCommand.getInstance();
        }
    },
    ADMIN_EDIT_DISH_REDIRECT {
        {
            this.command = EditDishRedirectCommand.getInstance();
        }
    },
    ADMIN_DELETE_DISH {
        {
            this.command = DeleteDishCommand.getInstance();
        }
    },
    ADMIN_ORDERS_REDIRECT {
        {
            this.command = OrdersRedirectCommand.getInstance();
        }
    },
    ADMIN_CREATE_COURIER_REDIRECT {
        {
            this.command = CreateCourierRedirectCommand.getInstance();
        }
    },
    ADMIN_COURIERS_REDIRECT {
        {
            this.command = CouriersRedirectCommand.getInstance();
        }
    },
    ADMIN_FIND_COURIERS {
        {
            this.command = FindCouriersCommand.getInstance();
        }
    },
    ADMIN_CHOOSE_COURIER {
        {
            this.command = ChooseCourierCommand.getInstance();
        }
    },
    ADMIN_DELETE_COURIER {
        {
            this.command = DeleteCourierCommand.getInstance();
        }
    },




    COURIER_ORDERS_REDIRECT {
        {
            this.command = CourierOrdersRedirectCommand.getInstance();
        }
    },
    COURIER_DONE_ORDER {
        {
            this.command = DoneOrderCommand.getInstance();
        }
    };
    Command command;
    public Command getCurrentCommand () {
        return command;
    }

}
