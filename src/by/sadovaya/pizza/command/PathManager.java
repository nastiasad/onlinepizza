package by.sadovaya.pizza.command;

import java.util.ResourceBundle;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class PathManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.path");
    private PathManager() {}
    public static String getProperty (String key) {
        return resourceBundle.getString(key);
    }
}
