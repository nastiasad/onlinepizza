package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.encryption.Encryption;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class SignupCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(SignupCommand.class);
    private static Command instance = new SignupCommand();
    private SignupCommand() {}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String name = request.getParameter("name");
        String surname = request.getParameter("surname");
        String mail = request.getParameter("mail");
        String login = request.getParameter("login");
        String password = Encryption.encryptPassword(request.getParameter("password"));

        try {
            long userId;
            UserStatus userStatus;
            if (request.getParameter("create-client") != null) {
                userStatus = UserStatus.ADMIN;
                UserServiceImpl.getInstance().create(name, surname, mail, login, password, UserStatus.COURIER);
                userId = (long) request.getSession().getAttribute("currentUserId");
                List<User> couriers = UserServiceImpl.getInstance().getAllCouriers();
                request.setAttribute("couriers", couriers);
                page = PathManager.getProperty("page.admin.couriers");
            } else {
                userStatus = UserStatus.CLIENT;
                userId = UserServiceImpl.getInstance().create(name, surname, mail, login, password, userStatus);
                page = PathManager.getProperty("page.main");
            }
            request.getSession().setAttribute("status", userStatus);
            request.getSession().setAttribute("currentUserId", userId);
        } catch (ServiceException e) {
            LOGGER.info("Cannot save user to db", e);
            request.setAttribute("errorSignupMessage",
                    PageContentManager.getProperty("message.signupError"));
            page = PathManager.getProperty("page.signup");
        }
        return page;
    }
}