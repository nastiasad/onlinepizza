package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created by Анастасия on 21.02.2016.
 */
public class LogoutCommand implements Command {
    private static Command instance = new LogoutCommand();
    private LogoutCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().invalidate();
        PageContentManager.setLocale(new Locale("ru", "RU"));
        return PathManager.getProperty("page.main");
    }
}
