package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PathManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class EmptyCommand implements Command {
    private static EmptyCommand instance = new EmptyCommand();
    private EmptyCommand(){}
    public static EmptyCommand getInstance(){
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        return PathManager.getProperty("page.main");
    }
}
