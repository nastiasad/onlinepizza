package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by anastasiya on 15.03.16.
 */
public class DishesRedirectCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(DishesRedirectCommand.class);
    private static Command instance = new DishesRedirectCommand();
    private DishesRedirectCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        DishType dishType;
        if (request.getParameter("dishType") != null) {
            dishType = DishType.valueOf(request.getParameter("dishType").toUpperCase());
        } else {
            dishType = (DishType) request.getAttribute("dishType");
        }
        try {
            List<Dish> dishes = DishServiceImpl.getInstance().getByType(dishType);
            if(dishes.isEmpty()) {
                request.setAttribute("allDishesError", PageContentManager.getProperty("message.allDishesError"));
            } else {
                request.setAttribute("allDishes", dishes);
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in DishesRedirectCommand", e);
            request.setAttribute("allDishesError", PageContentManager.getProperty("message.allDishesError"));
        }
        UserStatus status = (UserStatus) request.getSession().getAttribute("status");;
        String strStatus;
        if (status == UserStatus.ADMIN) {
            strStatus = "admin";
        } else {
            strStatus = "client";
        }
        return PathManager.getProperty("page." + strStatus + ".dishes");
    }
}
