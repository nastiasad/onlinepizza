package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created by anastasiya on 19.03.16.
 */
public class ChangeLocaleCommand implements Command {
    private static Command instance = new ChangeLocaleCommand();
    private ChangeLocaleCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String localeStr = null;
        String loc = request.getParameter("locale");
        Locale locale;


        switch (loc.toLowerCase()) {
            case "eng":
                locale = new Locale("en", "US");
                localeStr = "en_US";
                break;
            case "ru":
                locale = new Locale("ru", "RU");
                localeStr = "ru_RU";
                break;
            default:
                locale = new Locale("ru", "RU");
        }
        PageContentManager.setLocale(locale);
        request.getSession().setAttribute("locale", localeStr);
        return PathManager.getProperty("page.main");
    }
}
