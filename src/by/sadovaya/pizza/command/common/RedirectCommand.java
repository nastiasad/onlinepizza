package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PathManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class RedirectCommand implements Command {
    String path;
    public RedirectCommand(String path) {
        this.path = path;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = PathManager.getProperty(path);
        return page;
    }
}
