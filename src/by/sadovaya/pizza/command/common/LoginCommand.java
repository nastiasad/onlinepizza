package by.sadovaya.pizza.command.common;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.encryption.Encryption;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class LoginCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(LoginCommand.class);
    private static Command instance = new LoginCommand();
    private LoginCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String login = request.getParameter("login");
        String password = Encryption.encryptPassword(request.getParameter("password"));

        try {
            User user = UserServiceImpl.getInstance().checkLoginPassword(login, password);
            if (user != null && user.getStatus() != UserStatus.FIRED_COURIER) {
                request.getSession().setAttribute("status", user.getStatus());
                request.getSession().setAttribute("currentUserId", user.getId());
                page = PathManager.getProperty("page.main");
            } else {
                request.setAttribute("errorLoginMessage",
                        PageContentManager.getProperty("message.loginError"));
                page = PathManager.getProperty("page.main");
            }
        } catch (ServiceException e) {
            LOGGER.info("Exception in LoginCommand", e);
            request.setAttribute("errorUnknownLoginMessage",
                    PageContentManager.getProperty("message.unknownLoginError"));
        }
        return page;
    }
}
