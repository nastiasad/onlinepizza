package by.sadovaya.pizza.command;

import by.sadovaya.pizza.command.common.EmptyCommand;

/**
 * Created by Анастасия on 03.02.2016.
 */
public class CommandFactory {
    public static Command defineCommand (String commandString) {
        if (commandString == null || commandString.isEmpty()) {
            return EmptyCommand.getInstance();
        }
        CommandEnum currentEnum = CommandEnum.valueOf(commandString.toUpperCase().replace('-', '_'));
        return currentEnum.getCurrentCommand();
    }
}
