package by.sadovaya.pizza.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 03.02.2016.
 */
public interface Command {
    /**
     * Executes command
     * @param request
     * @return path of the page
     */
    String execute (HttpServletRequest request);
}
