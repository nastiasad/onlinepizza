package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.HashSet;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class CheckoutCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(CheckoutCommand.class);
    private static Command instance = new CheckoutCommand();
    private CheckoutCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        int house = Integer.parseInt(request.getParameter("house"));
        int flat = Integer.parseInt(request.getParameter("flat"));

        long userId = (long) request.getSession().getAttribute("currentUserId");
        Calendar curTime = Calendar.getInstance();
        try {
            HashSet<Dish> dishes = (HashSet<Dish>) request.getSession().getAttribute("tempUserOrder");
            OrderServiceImpl.getInstance().saveOrder(city, street, house, flat, userId, curTime, dishes);
            request.getSession().setAttribute("tempUserOrder", null);
            page = PathManager.getProperty("page.main");
        } catch (ServiceException e) {
            LOGGER.error("Exception in CheckoutCommand", e);
            request.setAttribute("checkoutError", PageContentManager.getProperty("message.checkoutError"));
            page=PathManager.getProperty("page.client.checkout");
        }
        return page;
    }
}
