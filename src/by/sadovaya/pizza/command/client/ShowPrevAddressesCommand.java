package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.AddressServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Анастасия on 17.02.2016.
 */
public class ShowPrevAddressesCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(ShowPrevAddressesCommand.class);
    private static Command instance = new ShowPrevAddressesCommand();
    private ShowPrevAddressesCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page;
        long userId = (long)request.getSession().getAttribute("currentUserId");
        try {
            List<Address> addresses = AddressServiceImpl.getInstance().getAddresses(userId);
            if (addresses.size() != 0) {
                request.setAttribute("addresses", addresses);
                page = PathManager.getProperty("page.client.prev-addresses");
            } else {
                request.setAttribute("prevAddressesNotFound", PageContentManager.getProperty("message.noPrevAddresses"));
                page = PathManager.getProperty("page.client.checkout");
            }

        } catch (ServiceException e) {
            LOGGER.error("Exception in ShowPrevAddressesCommand", e);
            request.setAttribute("prevAddressesNotFound", PageContentManager.getProperty("message.noPrevAddresses"));
            page = PathManager.getProperty("page.client.checkout");
        }
        return page;
    }
}
