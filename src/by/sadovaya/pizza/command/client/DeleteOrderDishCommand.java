package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;

/**
 * Created by Анастасия on 01.03.2016.
 */
public class DeleteOrderDishCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(DeleteOrderDishCommand.class);
    private static Command instance = new DeleteOrderDishCommand();
    private DeleteOrderDishCommand (){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long dishId = Long.parseLong(request.getParameter("dishId"));
        HashSet<Dish> dishes = (HashSet<Dish>) request.getSession().getAttribute("tempUserOrder");
        boolean forDelete = false;
        for (Dish dish: dishes) {
            if (dish.getId() == dishId) {
                if (dish.getAmount() > 1) {
                    dish.decAmount();
                } else {
                    forDelete = true;
                }
            }
        }
        if (forDelete) {
            try {
                Dish dish = DishServiceImpl.getInstance().getById(dishId);
                dishes.remove(dish);
            } catch (ServiceException e) {
                LOGGER.error("Exception in DeleteOrderDishCommand", e);
                request.setAttribute("deleteOrderDishError", PageContentManager.getProperty("message.deleteOrderDish"));
            }
        }

        request.getSession().setAttribute("tempUserOrder", dishes);
        return CurOrderRedirectCommand.getInstance().execute(request);
    }
}
