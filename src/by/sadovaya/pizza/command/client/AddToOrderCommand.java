package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.DishServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Анастасия on 15.02.2016.
 */
public class AddToOrderCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(AddToOrderCommand.class);
    private static Command instance = new AddToOrderCommand();
    private AddToOrderCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long dishId = Long.parseLong(request.getParameter("addToOrderId"));
        Dish dish = null;
        try {
            dish = DishServiceImpl.getInstance().getById(dishId);

            HashSet<Dish> tempUserOrder;
            if (request.getSession().getAttribute("tempUserOrder") == null) {
                tempUserOrder = new HashSet<>();
            } else {
                tempUserOrder = (HashSet<Dish>) request.getSession().getAttribute("tempUserOrder");
            }
            if (tempUserOrder.contains(dish)) {
                for (Dish orderDish: tempUserOrder) {
                    if (orderDish.equals(dish)) {
                        orderDish.incAmount();
                    }
                }
            } else {
                tempUserOrder.add(dish);
            }
            request.getSession().setAttribute("tempUserOrder", tempUserOrder);
        } catch (ServiceException e1) {
            LOGGER.error("Exception in AddToOrderCommand in temp user order", e1);
            request.setAttribute("tempUserOrderError", PageContentManager.getProperty("message.tempUserOrder"));
        }
        try {
            List<Dish> dishes;
            if (dish != null) {
                DishType dishType = dish.getType();
                dishes = DishServiceImpl.getInstance().getByType(dishType);
            } else {
                dishes = DishServiceImpl.getInstance().getByType(DishType.PIZZA);
            }

            if (dishes.isEmpty()) {
                request.setAttribute("allDishesError", PageContentManager.getProperty("message.allDishesError"));
            }
            request.setAttribute("allDishes", dishes);
        } catch (ServiceException e) {
            LOGGER.error("Exception in AddToOrderCommand", e);
            request.setAttribute("allDishesError", PageContentManager.getProperty("message.allDishesError"));
        }
        return PathManager.getProperty("page.client.dishes");
    }
}
