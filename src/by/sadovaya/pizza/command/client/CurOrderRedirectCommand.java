package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Dish;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class CurOrderRedirectCommand implements Command {
    private static Command instance = new CurOrderRedirectCommand();
    private CurOrderRedirectCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        HashSet<Dish> dishes = (HashSet<Dish>) request.getSession().getAttribute("tempUserOrder");
        if (dishes != null && dishes.size() > 0) {
            int orderPrice = 0;
            for (Dish dish : dishes) {
                orderPrice += dish.getAmount() * dish.getCost();
            }
            request.setAttribute("orderPrice", orderPrice);
        } else {
            request.setAttribute("curOrderError", PageContentManager.getProperty("message.curOrder"));
        }
        return PathManager.getProperty("page.client.cur-order");
    }
}
