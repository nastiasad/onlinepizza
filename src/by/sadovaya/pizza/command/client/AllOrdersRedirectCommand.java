package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by anastasiya on 18.03.16.
 */
public class AllOrdersRedirectCommand implements Command{
    private static final Logger LOGGER = Logger.getLogger(AllOrdersRedirectCommand.class);
    private static Command instance = new AllOrdersRedirectCommand();
    private AllOrdersRedirectCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long clientId = (long) request.getSession().getAttribute("currentUserId");
        try {
            List<Order> orders = OrderServiceImpl.getInstance().getOrdersByClientId(clientId);
            if (orders.size() > 0) {
                request.setAttribute("orders", orders);
            } else {
                request.setAttribute("ordersError", PageContentManager.getProperty("message.clientOrders"));
            }
        } catch (ServiceException e) {
            LOGGER.error("Exception in AllOrdersRedirectCommand", e);
            request.setAttribute("ordersError", PageContentManager.getProperty("message.clientOrders"));
        }
        return PathManager.getProperty("page.client.all-orders");

    }
}
