package by.sadovaya.pizza.command.client;

import by.sadovaya.pizza.command.Command;
import by.sadovaya.pizza.command.PathManager;
import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.impl.AddressServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Анастасия on 18.02.2016.
 */
public class ChooseAddressCommand implements Command {
    private static final Logger LOGGER = Logger.getLogger(ChooseAddressCommand.class);
    private static Command instance = new ChooseAddressCommand();
    private ChooseAddressCommand(){}
    public static Command getInstance() {
        return instance;
    }

    @Override
    public String execute(HttpServletRequest request) {
        long addressId = Long.parseLong(request.getParameter("addrId"));
        try {
            Address address = AddressServiceImpl.getInstance().getById(addressId);
            request.setAttribute("addr", address);
        } catch (ServiceException e) {
            LOGGER.error("Exception in ChooseAddressCommand", e);
        }
        return PathManager.getProperty("page.client.checkout");
    }
}
