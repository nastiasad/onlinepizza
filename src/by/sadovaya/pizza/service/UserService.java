package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.ServiceException;

import java.util.List;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface UserService {
    /**
     * Forms a User object from parameters, checks whether the object is valid and sends it to save in dao
     * @param name
     * @param surname
     * @param mail
     * @param login
     * @param password
     * @param status
     * @return auto generated id
     * @throws ServiceException
     */
    long create (String name, String surname, String mail, String login, String password, UserStatus status)
            throws ServiceException;

    /**
     * Gets from dao User object by user id
     * @param userId
     * @return User object
     * @throws ServiceException
     */
    User getById (long userId) throws ServiceException;

    /**
     * Sends login and password to dao to check, if the user exists
     * @param login
     * @param password
     * @return User object, if he exists in database, and null otherwise
     * @throws ServiceException
     */
    User checkLoginPassword (String login, String password) throws ServiceException;

    /**
     * Gets all Users, that have UserStatus.Courier, from dao
     * @return list of User objects
     * @throws ServiceException
     */
    List<User> getAllCouriers () throws ServiceException;

    /**
     * Sends courier id from view to dao to set CourierStatus.FIRED
     * @param courierId
     * @return boolean value whether status was changed or not
     * @throws ServiceException
     */
    boolean fireCourier (long courierId) throws ServiceException;
}
