package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.ServiceException;
import java.util.List;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface CourierService {
    /**
     * Gets the list of couriers, which are free right now, from dao
     * @return the list of users, that are couriers
     * @throws ServiceException
     */
    List<User> getFreeCouriers () throws ServiceException;

    /**
     * Sends courier status and id from view to dao to update current courier status
     * @param courierId
     * @param status
     * @throws ServiceException
     */
    void updateStatus (long courierId, CourierStatus status) throws ServiceException;

    /**
     * Sends order id, courier id and status from view to dao to set the courier for order
     * @param orderId
     * @param courierId
     * @param courierStatus
     * @throws ServiceException
     */
    void setCourierForOrder (long orderId, long courierId, CourierStatus courierStatus) throws ServiceException;

    /**
     * Sends order id to dao to remove the row from database
     * @param courierId
     * @throws ServiceException
     */
    void removeCourierOrder (long courierId) throws ServiceException;
}
