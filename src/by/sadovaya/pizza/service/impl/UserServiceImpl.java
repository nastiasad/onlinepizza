package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.dao.impl.UserDaoImpl;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.UserService;
import by.sadovaya.pizza.validation.ValidateUtil;

import java.util.List;

/**
 * Created by Анастасия on 07.02.2016.
 */
public class UserServiceImpl implements UserService {
    private static UserService instance = new UserServiceImpl();
    private UserServiceImpl(){}
    public static UserService getInstance (){
        return instance;
    }

    @Override
    public long create (String name, String surname, String mail, String login, String password, UserStatus status)
            throws ServiceException {
        User user = new User (name, surname, mail, login, password, status);
        if(!ValidateUtil.validateUser(user)) {
            throw new ServiceException("Exception in UserServiceImpl.create(), cause the User object is invalid");
        }
        try {
            long userId = UserDaoImpl.getInstance().save(user);
            return userId;
        } catch (DaoException e) {
            throw new ServiceException("Exception in UserServiceImpl.create()", e);
        }
    }

    @Override
    public User getById (long userId) throws ServiceException {
        try {
            return UserDaoImpl.getInstance().getById(userId);
        } catch (DaoException e) {
            throw new ServiceException("Exception in UserServiceImpl.getById()", e);
        }
    }

    @Override
    public User checkLoginPassword (String login, String password) throws ServiceException {
        try {
            return UserDaoImpl.getInstance().checkLoginPassword(login, password);
        } catch (DaoException e) {
            throw new ServiceException("Exception in UserServiceImpl.checkLoginPassword()", e);
        }
    }

    @Override
    public List<User> getAllCouriers () throws ServiceException {
        try {
            return UserDaoImpl.getInstance().getAllCouriers();
        } catch (DaoException e) {
            throw new ServiceException("Exception in UserServiceImpl.getAllCouriers()", e);
        }
    }

    @Override
    public boolean fireCourier (long courierId) throws ServiceException {
        try {
            if (OrderServiceImpl.getInstance().getOrdersByCourierId(courierId).isEmpty()) {
                UserDaoImpl.getInstance().changeStatus(UserStatus.FIRED_COURIER, courierId);
                CourierServiceImpl.getInstance().removeCourierOrder(courierId);
                return true;
            } else {
                return false;
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in UserServiceImpl.fireCourier()", e);
        }
    }
}
