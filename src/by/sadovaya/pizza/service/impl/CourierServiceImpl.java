package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.dao.impl.CourierDaoImpl;
import by.sadovaya.pizza.dao.impl.UserDaoImpl;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.CourierService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Анастасия on 29.02.2016.
 */
public class CourierServiceImpl implements CourierService{
    private static CourierService instance = new CourierServiceImpl();
    private CourierServiceImpl(){}
    public static CourierService getInstance() {
        return instance;
    }

    @Override
    public List<User> getFreeCouriers () throws ServiceException {
        try {
            List<Long> courierIds = CourierDaoImpl.getInstance().getAllFree();
            List<User> couriers = new ArrayList<>();
            for (long courierId: courierIds) {
                couriers.add(UserDaoImpl.getInstance().getById(courierId));
            }
            return couriers;
        } catch (DaoException e) {
            throw new ServiceException("Exception in CourierServiceImpl.getFreeCouriers()", e);
        }
    }

    @Override
    public void updateStatus (long courierId, CourierStatus status) throws ServiceException {
        try {
            CourierDaoImpl.getInstance().updateStatus(courierId, status);
        } catch (DaoException e) {
            throw new ServiceException("Exception in CourierServiceImpl.getCourierStatus()", e);
        }
    }

    @Override
    public void setCourierForOrder (long orderId, long courierId, CourierStatus courierStatus) throws ServiceException {
        try {
            CourierDaoImpl.getInstance().setCourierForOrder(orderId, courierId, courierStatus);
        } catch (DaoException e) {
            throw new ServiceException("Exception in CourierServiceImpl.setCourierForOrder()", e);
        }
    }

    @Override
    public void removeCourierOrder(long courierId) throws ServiceException {
        try {
            CourierDaoImpl.getInstance().removeCourierStatus(courierId);
        } catch (DaoException e) {
            throw new ServiceException("Exception in CourierDaoImpl.removeCourierOrder(long courierId");
        }
    }
}
