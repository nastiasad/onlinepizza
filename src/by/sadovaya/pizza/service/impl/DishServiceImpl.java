package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.dao.impl.DishDaoImpl;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.OrderStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.DishService;
import by.sadovaya.pizza.validation.ValidateUtil;

import java.util.List;
import java.util.Set;

/**
 * Created by Анастасия on 11.02.2016.
 */
public class DishServiceImpl implements DishService {
    private static DishService instance = new DishServiceImpl();
    private DishServiceImpl(){}
    public static DishService getInstance() {
        return instance;
    }

    @Override
    public void createDish (String name, DishType type, String description, List<String> ingredients,
                            int weight, int cost) throws ServiceException {
        Dish dish = new Dish (name, type, description, ingredients, weight, cost);
        if (!ValidateUtil.validateDish(dish)) {
            throw new ServiceException("Exception in DishServiceImpl.createDish() cause the Dish object is invalid");
        }
        try {
            DishDaoImpl.getInstance().save(dish);
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.createDish()", e);
        }
    }

    @Override
    public void updateDish (String name, DishType type, String description, List<String> ingredients,
                            int weight, int cost, long id) throws ServiceException {
        Dish dish = new Dish(id, name, type, description, ingredients, weight, cost);
        if (!ValidateUtil.validateDish(dish)) {
            throw new ServiceException("Exception in DishServiceImpl.updateDish() cause the Dish object is invalid");
        }
        try {
            DishDaoImpl.getInstance().update(dish);
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.updateDish()", e);
        }
    }

    @Override
    public boolean deleteDish (long id) throws ServiceException {
        try {
            boolean isDeleted = false;
            if (!DishDaoImpl.getInstance().findInOrders(id)) {
                DishDaoImpl.getInstance().moveToArchive(id);
                isDeleted = true;
            }
            return isDeleted;
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.deleteDish()", e);
        }
    }

    @Override
    public List<Dish> getByType (DishType type) throws ServiceException {
        try {
            return DishDaoImpl.getInstance().getByType(type);
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.getByType(DishType type)", e);
        }
    }

    @Override
    public Dish getById (long id) throws ServiceException {
        try {
            return DishDaoImpl.getInstance().getById(id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.getById()", e);
        }
    }

    @Override
    public Set<Dish> getOrderDishes (long orderId, OrderStatus status) throws ServiceException {
        try {
            return DishDaoImpl.getInstance().getOrderDishes(orderId, status);
        } catch (DaoException e) {
            throw new ServiceException("Exception in DishServiceImpl.getOrderDishes()", e);
        }
    }
}
