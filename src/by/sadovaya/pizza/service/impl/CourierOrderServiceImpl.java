package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.entity.*;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.CourierOrderService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Анастасия on 28.02.2016.
 */
public class CourierOrderServiceImpl implements CourierOrderService {
    private static CourierOrderService instance = new CourierOrderServiceImpl();
    private CourierOrderServiceImpl(){}
    public static CourierOrderService getInstance() {
        return instance;
    }

    @Override
    public List<CourierOrder> getCourierOrders (long courierId) throws ServiceException {
        try {
            List<CourierOrder> courierOrders = new ArrayList<>();
            List<Order> orders = OrderServiceImpl.getInstance().getOrdersByCourierId(courierId);
            for (Order order: orders) {
                Address address = AddressServiceImpl.getInstance().getById(order.getAddressId());
                User user = UserServiceImpl.getInstance().getById(address.getUserId());
                CourierOrder courierOrder = new CourierOrder(order, address, user.getName());
                int total = 0;
                for (Dish dish: courierOrder.getOrder().getDishes()) {
                    total += dish.getCost() * dish.getAmount();
                }
                courierOrder.setTotal(total);
                courierOrders.add(courierOrder);
            }
            return courierOrders;
        } catch (ServiceException e) {
            throw new ServiceException("Exception in CourierOrderServiceImpl.getCourierOrders()", e);
        }
    }
}
