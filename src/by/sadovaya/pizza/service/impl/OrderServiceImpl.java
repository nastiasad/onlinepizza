package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.dao.impl.DishDaoImpl;
import by.sadovaya.pizza.dao.impl.OrderDaoImpl;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.OrderService;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class OrderServiceImpl implements OrderService {
    private static OrderService instance = new OrderServiceImpl();
    private OrderServiceImpl(){}
    public static OrderService getInstance() {
        return instance;
    }

    @Override
    public void saveOrder (String city, String street, int house, int flat, long userId,
                           Calendar dateTime, HashSet<Dish> dishes) throws ServiceException {
        long addressId = AddressServiceImpl.getInstance().saveAddress(city, street, house, flat, userId);
        Order order = new Order(dateTime, addressId, dishes);
        try {
            OrderDaoImpl.getInstance().save(order);
        } catch (DaoException e) {
            throw new ServiceException("Exception in OrderServiceImpl.save()", e);
        }
    }

    @Override
    public List<Order> getAllNew () throws ServiceException {
        try {
            return OrderDaoImpl.getInstance().getAllNew();
        } catch (DaoException e) {
            throw new ServiceException("Exception in OrderServiceImpl.getAllNew()", e);
        }
    }

    @Override
    public List<Order> getOrdersByCourierId(long courierId) throws ServiceException {
        try {
            List<Order> orders = OrderDaoImpl.getInstance().getByCourierId(courierId);
            for (Order order: orders) {
                order.setDishes(DishServiceImpl.getInstance().getOrderDishes(order.getId(), order.getStatus()));
            }
            return orders;
        } catch (DaoException e) {
            throw new ServiceException("Exception in OrderServiceImpl.getByCourierId(long courierId)", e);
        }
    }

    @Override
    public List<Order> getOrdersByClientId (long clientId) throws ServiceException {
        try {
            List<Order> orders = OrderDaoImpl.getInstance().getByClientId(clientId);
            for (Order order: orders) {
                order.setDishes(DishDaoImpl.getInstance().getOrderDishes(order.getId(), order.getStatus()));
            }
            return orders;
        } catch (DaoException e) {
            throw new ServiceException("Exception in OrderServiceImpl.getByClientId(long clientId)", e);
        }
    }

    @Override
    public void makeOrderDone (long orderId, long courierId) throws ServiceException {
        try {
            OrderDaoImpl.getInstance().moveToArchive(orderId);
            CourierServiceImpl.getInstance().updateStatus(courierId, CourierStatus.FREE);
        } catch (DaoException e) {
            throw new ServiceException("Exception in OrderServiceImpl.moveToArchive()", e);
        }
    }
}
