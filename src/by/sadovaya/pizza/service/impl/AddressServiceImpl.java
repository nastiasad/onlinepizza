package by.sadovaya.pizza.service.impl;

import by.sadovaya.pizza.dao.impl.AddressDaoImpl;
import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.exception.ServiceException;
import by.sadovaya.pizza.service.AddressService;
import by.sadovaya.pizza.validation.ValidateUtil;

import java.util.List;

/**
 * Created by Анастасия on 17.02.2016.
 */
public class AddressServiceImpl implements AddressService{
    private static AddressService instance = new AddressServiceImpl();
    private AddressServiceImpl(){}
    public static AddressService getInstance() {
        return instance;
    }

    @Override
    public long saveAddress (String city, String street, int house, int flat, long userId) throws ServiceException {
        Address address = new Address(city, street, house, flat, userId);
        if (!ValidateUtil.validateAddress(address)) {
            throw new ServiceException("Exception in AddressServiceImpl.save() cause the Address object is invalid");
        }
        try {
            return AddressDaoImpl.getInstance().save(address);
        } catch (DaoException e) {
            throw new ServiceException("Exception in AddressServiceImpl.save(Address address)", e);
        }
    }

    @Override
    public List<Address> getAddresses (long userId) throws ServiceException {
        try {
            return AddressDaoImpl.getInstance().getByUserId(userId);
        } catch (DaoException e) {
            throw new ServiceException("Exception in AddressServiceImpl.getByUserId (long userId)", e);
        }
    }

    @Override
    public Address getById (long id) throws ServiceException {
        try {
            return AddressDaoImpl.getInstance().getById(id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in AddressServiceImpl.getById(long id)", e);
        }
    }
}
