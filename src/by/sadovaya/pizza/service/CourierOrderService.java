package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.CourierOrder;
import by.sadovaya.pizza.exception.ServiceException;
import java.util.List;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface CourierOrderService {
    /**
     * Makes list of orders for courier
     * @param courierId
     * @return list of CourierOrder objects
     * @throws ServiceException
     */
    List<CourierOrder> getCourierOrders (long courierId) throws ServiceException;
}
