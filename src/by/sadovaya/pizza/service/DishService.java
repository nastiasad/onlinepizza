package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.OrderStatus;
import by.sadovaya.pizza.exception.ServiceException;

import java.util.List;
import java.util.Set;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface DishService {
    /**
     * Forms a Dish object from parameters, checks if the Dish object is valid and sends it to save in dao
     * @param name
     * @param type
     * @param description
     * @param ingredients
     * @param weight
     * @param cost
     * @throws ServiceException
     */
    void createDish (String name, DishType type, String description, List<String> ingredients,
                     int weight, int cost) throws ServiceException;

    /**
     * Forms a Dish object from parameters, checks if the Dish object is valid and sends it to update in dao
     * @param name
     * @param type
     * @param description
     * @param ingredients
     * @param weight
     * @param cost
     * @param id
     * @throws ServiceException
     */
    void updateDish (String name, DishType type, String description, List<String> ingredients,
                     int weight, int cost, long id) throws ServiceException;

    /**
     * Sends dish id to dao to delete it
     * @param id
     * @return boolean value whether it is deleted or not
     * @throws ServiceException
     */
    boolean deleteDish (long id) throws ServiceException;

    /**
     * Gets list of Dish objects by dish type from dao
     * @param type
     * @return list of Dish objects
     * @throws ServiceException
     */
    List<Dish> getByType (DishType type) throws ServiceException;

    /**
     * Gets Dish object by dish id from dao
     * @param id
     * @return Dish object
     * @throws ServiceException
     */
    Dish getById (long id) throws ServiceException;

    /**
     * Gets set of Dish objects from dao, which are in order, by order id and status
     * @param orderId
     * @param status
     * @return set of Dish orders
     * @throws ServiceException
     */
    Set<Dish> getOrderDishes (long orderId, OrderStatus status) throws ServiceException;
}
