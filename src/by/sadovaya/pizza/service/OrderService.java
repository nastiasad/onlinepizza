package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.exception.ServiceException;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface OrderService {
    /**
     * Forms an Order object from parameters and sends it to save in dao
     * @param city
     * @param street
     * @param house
     * @param flat
     * @param userId
     * @param dateTime
     * @param dishes
     * @throws ServiceException
     */
    void saveOrder (String city, String street, int house, int flat, long userId,
                    Calendar dateTime, HashSet<Dish> dishes) throws ServiceException;

    /**
     * Gets from dao all orders, which have OrderStatus.NEW
     * @return list of Order objects
     * @throws ServiceException
     */
    List<Order> getAllNew () throws ServiceException;

    /**
     * Gets from dao all orders for courier by courier id
     * @param courierId
     * @return list of Order objects
     * @throws ServiceException
     */
    List<Order> getOrdersByCourierId(long courierId) throws ServiceException;

    /**
     * Gets all client orders from dao by client id
     * @param clientId
     * @return list of Order objects
     * @throws ServiceException
     */
    List<Order> getOrdersByClientId (long clientId) throws ServiceException;

    /**
     * Sends order from view to dao to make it done and sends courier id from view to dao to set CourierStatus.FREE
     * @param orderId
     * @param courierId
     * @throws ServiceException
     */
    void makeOrderDone (long orderId, long courierId) throws ServiceException;
}
