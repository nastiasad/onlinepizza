package by.sadovaya.pizza.service;

import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.ServiceException;
import java.util.List;

/**
 * Created by anastasiya on 20.03.16.
 */
public interface AddressService {
    /**
     * Forms an Address object from parameters, checks if the Address object is valid and sends it to save in dao
     * @param city
     * @param street
     * @param house
     * @param flat
     * @param userId
     * @return auto generated id
     * @throws ServiceException
     */
    long saveAddress (String city, String street, int house, int flat, long userId) throws ServiceException;

    /**
     * Gets the list of addresses with appropriate user id from dao
     * @param userId
     * @return list of addresses
     * @throws ServiceException
     */
    List<Address> getAddresses (long userId) throws ServiceException;

    /**
     * Gets address from dao by its id
     * @param id
     * @return Address object
     * @throws ServiceException
     */
    Address getById (long id) throws ServiceException;
}
