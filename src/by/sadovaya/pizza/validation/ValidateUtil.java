package by.sadovaya.pizza.validation;

import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.entity.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by anastasiya on 04.04.16.
 */
public class ValidateUtil {
    private static final String STRING_FIELD = "^.{3,30}$";
    private static final String MAIL = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
    private static final String LOGIN = "^[a-zA-Z0-9_]{4,30}$";

    public static boolean validateUser (User user) {
        return (check(user.getName(), STRING_FIELD) &&
                check(user.getSurname(), STRING_FIELD) &&
                check(user.getMail(), MAIL) &&
                check(user.getLogin(), LOGIN));
    }


    public static boolean validateDish (Dish dish) {
        return (check(dish.getName(), STRING_FIELD) &&
                (dish.getWeight() >= 1) &&
                (dish.getCost() >= 0));
    }

    public static boolean validateAddress (Address address) {
        return (check(address.getStreet(), STRING_FIELD) &&
                (address.getHouse() >= 1) &&
                (address.getFlat() >= 1));
    }


    private static boolean check (String str, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}
