package by.sadovaya.pizza.pool;

import by.sadovaya.pizza.exception.DaoException;
import org.apache.log4j.Logger;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by Анастасия on 06.02.2016.
 */
public class ConnectionPool {
    private static final Logger LOGGER = Logger.getLogger(ConnectionPool.class);

    private LinkedBlockingDeque<ProxyConnection> connectionQueue;

    private static final String URL = DBPropertyManager.getProperty("url");
    private static final String USER = DBPropertyManager.getProperty("user");
    private static final String PASSWORD = DBPropertyManager.getProperty("password");
    private static final int POOL_SIZE = Integer.parseInt(DBPropertyManager.getProperty("poolSize"));



    private ConnectionPool (int poolSize) {
        connectionQueue = new LinkedBlockingDeque<ProxyConnection>();
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOGGER.fatal("ConnectionPool(int poolSize", e);
            throw new RuntimeException(e);
        }

        for (int i = 0; i < poolSize; i++){
            connectionQueue.offer(createConnection());
        }
    }

    public static ConnectionPool getInstance () {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final ConnectionPool INSTANCE = new ConnectionPool(POOL_SIZE);
    }

    private ProxyConnection createConnection () {
        ProxyConnection pc = null;
        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            pc = new ProxyConnection(connection);
        } catch (SQLException e) {
            LOGGER.error("createConnection()", e);
        }
        return pc;
    }

    public ProxyConnection getConnection () throws DaoException {
        ProxyConnection connection;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            throw new DaoException("getConnection", e);
        }
        return connection;
    }

    public void closeConnection (ProxyConnection connection) {
        try {
            connectionQueue.put(connection);
        } catch (InterruptedException e) {
            LOGGER.error("closeConnection(ProxyConnection conn)", e);
            connectionQueue.offer(createConnection());
        }
    }

    @PreDestroy
    public void closeAll (){
        connectionQueue.forEach(ProxyConnection::doClose);
    }
}
