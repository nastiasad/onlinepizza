package by.sadovaya.pizza.pool;

import java.util.ResourceBundle;

/**
 * Created by Анастасия on 06.02.2016.
 */
public class DBPropertyManager {
    private static final ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private DBPropertyManager(){}

    public static String getProperty (String key){
        return resourceBundle.getString(key);
    }
}
