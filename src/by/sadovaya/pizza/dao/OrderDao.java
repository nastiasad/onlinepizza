package by.sadovaya.pizza.dao;

import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.exception.DaoException;

import java.util.List;

/**
 * Created by Анастасия on 16.02.2016.
 */
public interface OrderDao {
    /**
     * Saves Order object in database
     * @param order
     * @throws DaoException
     */
    void save(Order order) throws DaoException;

    /**
     * Gets all orders from database with OrderStatus.NEW
     * @return list of Order objects
     * @throws DaoException
     */
    List<Order> getAllNew () throws DaoException;

    /**
     * Gets all orders from database for courier by his id
     * @param courierId
     * @return list of Order objects
     * @throws DaoException
     */
    List<Order> getByCourierId(long courierId) throws DaoException;

    /**
     * Gets all orders from all tables containing orders for client by his id
     * @param clientId
     * @return list of Order objects
     * @throws DaoException
     */
    List<Order> getByClientId(long clientId) throws DaoException;

    /**
     * Moves order by its id to archive tables
     * @param orderId
     * @throws DaoException
     */
    void moveToArchive (long orderId) throws DaoException;

}
