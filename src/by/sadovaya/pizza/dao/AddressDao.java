package by.sadovaya.pizza.dao;

import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.DaoException;

import java.util.List;

/**
 * Created by Анастасия on 17.02.2016.
 */
public interface AddressDao {
    /**
     * Saves Address object in database
     * @param address
     * @return auto generated id
     * @throws DaoException
     */
    long save(Address address) throws DaoException;

    /**
     * Gets user's addresses from database
     * @param userId
     * @return list of Address objects
     * @throws DaoException
     */
    List<Address> getByUserId(long userId) throws DaoException;

    /**
     * Gets address by its id
     * @param id
     * @return Address object
     * @throws DaoException
     */
    Address getById (long id) throws DaoException;
}
