package by.sadovaya.pizza.dao.util;

import by.sadovaya.pizza.exception.DaoException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class ConnectionUtil {
    public static void rollbackConnection(Connection connection, String nameOfMethod) throws DaoException {
        try {
            connection.rollback();
            connection.setAutoCommit(true);
        } catch (SQLException e1) {
            throw new DaoException("Exception in " + nameOfMethod + " in connection.rollback() method", e1);
        }
    }

    public static void closeConnection (Connection connection, String nameOfMethod) throws DaoException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new DaoException("Exception in " + nameOfMethod + " in method connection.close()", e);
        }
    }
}
