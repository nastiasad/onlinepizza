package by.sadovaya.pizza.dao;

import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.DaoException;

import java.util.List;

/**
 * Created by Анастасия on 07.02.2016.
 */
public interface UserDao {
    /**
     * Saves User object in database
     * @param user
     * @return auto incremented id
     * @throws DaoException
     */
    long save (User user) throws DaoException;

    /**
     * Gets user from database  by its id
     * @param userId
     * @return User object
     * @throws DaoException
     */
    User getById (long userId) throws DaoException;

    /**
     * Checks whether the database contains a pair of login and password
     * @param login
     * @param password
     * @return User object if the match was found and null otherwise
     * @throws DaoException
     */
    User checkLoginPassword (String login, String password) throws DaoException;

    /**
     * Gets all users that have UserStatus.COURIER from database
     * @return list of User objects
     * @throws DaoException
     */
    List<User> getAllCouriers () throws DaoException;

    /**
     * Updates in database user status
     * @param status
     * @param userId
     * @throws DaoException
     */
    void changeStatus (UserStatus status, long userId) throws DaoException;
}
