package by.sadovaya.pizza.dao;

import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.DaoException;

import java.util.List;

/**
 * Created by Анастасия on 18.02.2016.
 */
public interface CourierDao {
    /**
     * Gets all courier ids that have CourierStatus.FREE
     * @return list of courier ids
     * @throws DaoException
     */
    List<Long> getAllFree () throws DaoException;

    /**
     * Updates courier status in database by courier id
     * @param courierId
     * @param status
     * @throws DaoException
     */
    void updateStatus (long courierId, CourierStatus status) throws DaoException;

    /**
     * Sets courier for order in database and updates courier status
     * @param courierId
     * @param orderId
     * @param courierStatus
     * @throws DaoException
     */
    void setCourierForOrder (long courierId, long orderId, CourierStatus courierStatus) throws DaoException;

    /**
     * Removes row from courier_status table when courier is fired
     * @param courierId
     * @throws DaoException
     */
    void removeCourierStatus (long courierId) throws DaoException;
}
