package by.sadovaya.pizza.dao;

import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.OrderStatus;
import by.sadovaya.pizza.exception.DaoException;

import java.util.List;
import java.util.Set;

/**
 * Created by Анастасия on 11.02.2016.
 */
public interface DishDao {
    /**
     * Saves Dish object in database
     * @param dish
     * @throws DaoException
     */
    void save(Dish dish) throws DaoException;

    /**
     * Updates Dish object in database
     * @param dish
     * @throws DaoException
     */
    void update(Dish dish) throws DaoException;

    /**
     * Moves dish by its id from 'dishes' table to 'archive_dishes' table
     * @param id
     * @throws DaoException
     */
    void moveToArchive(long id) throws DaoException;

    /**
     * Gets all dishes with particular type from database
     * @param type
     * @return list of Dish objects
     * @throws DaoException
     */
    List<Dish> getByType (DishType type) throws DaoException;

    /**
     * Gets dish by its id from database
     * @param id
     * @return Dish object
     * @throws DaoException
     */
    Dish getById(long id) throws DaoException;

    /**
     * Gets all dishes from particular order by its id. OrderStatus is used for getting order from one of the tables with orders
     * @param orderId
     * @param status
     * @return set of Dish objects
     * @throws DaoException
     */
    Set<Dish> getOrderDishes (long orderId, OrderStatus status) throws DaoException;

    /**
     * Searches for dish by its id in all current orders
     * @param dishId
     * @return boolean value whether the dish was found or not
     * @throws DaoException
     */
    boolean findInOrders (long dishId) throws DaoException;
}
