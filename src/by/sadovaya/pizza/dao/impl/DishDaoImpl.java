package by.sadovaya.pizza.dao.impl;

import by.sadovaya.pizza.dao.DishDao;
import by.sadovaya.pizza.dao.util.ConnectionUtil;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.OrderStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Анастасия on 11.02.2016.
 */
public class DishDaoImpl implements DishDao {
    private static final Logger LOGGER = Logger.getLogger(DishDaoImpl.class);
    private static DishDao instance = new DishDaoImpl();
    private DishDaoImpl (){}
    public static DishDao getInstance() {
        return instance;
    }

    private static final String INSERT_DISH = "INSERT INTO dishes (name, type, description, weight, cost) VALUES " +
            "(?, ?, ?, ?, ?)";
    private static final String INSERT_INGREDIENT = "INSERT INTO ingredients (name) VALUES (?) " +
            "ON DUPLICATE KEY UPDATE name=name";
    private static final String UPDATE_DISH = "UPDATE dishes SET name=?, type=?, " +
            "description=?, weight=?, cost=? WHERE id=?";
    private static final String DELETE_DISH_INGRED = "DELETE FROM dishes_ingredients WHERE dish_id=?";

    private static final String MOVE_TO_ARCHIVE = "INSERT INTO archive_dishes SELECT * FROM dishes WHERE id=?";
    private static final String MOVE_INGRED_TO_ARCHIVE = "INSERT INTO archive_dishes_ingreds " +
            "SELECT * FROM dishes_ingredients WHERE dish_id=?";
    private static final String DELETE_BY_ID = "DELETE FROM dishes WHERE id=?";

    private static final String GET_INGRED_ID_BY_NAME = "SELECT id FROM ingredients WHERE name=?";
    private static final String INSERT_DISH_INGREDIENT = "INSERT INTO dishes_ingredients (dish_id, ingredient_id) " +
            "VALUES (?, ?)";
    private static final String GET_DISHES_BY_TYPE = "SELECT * FROM dishes WHERE type=?";

    private static final String GET_DISH_INGREDIENTS =
            "SELECT i.name AS ingredient FROM dishes_ingredients di " +
            "LEFT JOIN ingredients i ON di.ingredient_id = i.id " +
            "WHERE di.dish_id=? AND di.ingredient_id IS NOT NULL " +
            "UNION " +
            "SELECT i.name AS ingredient FROM archive_dishes_ingreds adi " +
            "LEFT JOIN ingredients i ON adi.ingredient_id = i.id " +
            "WHERE adi.dish_id=? AND adi.ingredient_id IS NOT NULL";

    private static final String GET_BY_ID = "SELECT * FROM dishes WHERE id=?";

    private static final String GET_FOR_ORDER = "SELECT  d.id AS id, d.name AS name, d.type AS type, " +
            "d.description AS description, d.weight AS weight, d.cost AS cost, od.number_dishes AS number_dishes " +
            "FROM order_dish od LEFT JOIN dishes d ON od.dish_id = d.id " +
            "WHERE od.order_id=?";

    private static final String GET_FOR_ORDER_ARCHIVE = "SELECT  d.id AS id, d.name AS name, d.type AS type, " +
            "d.description AS description, d.weight AS weight, d.cost AS cost, od.number_dishes AS number_dishes " +
            "FROM archive_order_dish od LEFT JOIN dishes AS d ON od.dish_id = d.id " +
            "WHERE od.order_id=? AND d.id IS NOT NULL " +
            "UNION " +
            "SELECT  ad.id AS id, ad.name AS name, ad.type AS type, " +
            "ad.description AS description, ad.weight AS weight, ad.cost AS cost, od.number_dishes AS number_dishes " +
            "FROM archive_order_dish od LEFT JOIN archive_dishes ad ON od.dish_id = ad.id " +
            "WHERE od.order_id=? AND ad.id IS NOT NULL ";

    private static final String FIND_IN_ORDERS = "SELECT * FROM order_dish WHERE dish_id=?";

    @Override
    public void save(Dish dish) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_DISH,
                PreparedStatement.RETURN_GENERATED_KEYS)) {

            connection.setAutoCommit(false);
            //start of transaction
            long dishId = insertUpdateDish(statement, dish);
            dish.setId(dishId);
            insertUpdateDishIngredient(connection, dish);

            //end of transaction
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "DishDaoImpl.save(Dish dish)");
            throw new DaoException("Exception in DishDaoImpl.save(Dish dish)", e);

        } finally {
            ConnectionUtil.closeConnection(connection, "DishDaoImpl.save(Dish dish)");
        }
    }

    @Override
    public void update(Dish dish) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_DISH,
                PreparedStatement.RETURN_GENERATED_KEYS);
             PreparedStatement deleteStatement = connection.prepareStatement(DELETE_DISH_INGRED)) {
            connection.setAutoCommit(false);

            insertUpdateDish(statement, dish);

            deleteStatement.setLong(1, dish.getId());
            deleteStatement.executeUpdate();
            insertUpdateDishIngredient(connection, dish);

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "DishDaoImpl.update()");
            throw new DaoException("Exception in DishDaoImpl.update()", e);

        } finally {
            ConnectionUtil.closeConnection(connection, "DishDaoImpl.update()");
        }

    }

    @Override
    public void moveToArchive(long id) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement moveStatement = connection.prepareStatement(MOVE_TO_ARCHIVE);
             PreparedStatement moveIngredStatement = connection.prepareStatement(MOVE_INGRED_TO_ARCHIVE);
             PreparedStatement deleteStatement = connection.prepareStatement(DELETE_BY_ID);
             PreparedStatement deleteDishIngredStatment = connection.prepareStatement(DELETE_DISH_INGRED)){
            connection.setAutoCommit(false);

            moveStatement.setLong(1, id);
            moveStatement.executeUpdate();

            moveIngredStatement.setLong(1, id);
            moveIngredStatement.executeUpdate();

            deleteStatement.setLong(1, id);
            deleteStatement.executeUpdate();

            deleteDishIngredStatment.setLong(1, id);
            deleteDishIngredStatment.executeUpdate();

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "DishDaoImpl.moveToArchive()");
            throw new DaoException("Exception in DishDaoImpl.moveToArchive()", e);

        } finally {
            ConnectionUtil.closeConnection(connection, "DishDaoImpl.moveToArchive()");
        }
    }

    @Override
    public List<Dish> getByType(DishType type) throws DaoException {
        List<Dish> dishes = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement getDishesStatement = connection.prepareStatement(GET_DISHES_BY_TYPE)) {
            getDishesStatement.setString(1, type.toString());
            ResultSet getDishesRs = getDishesStatement.executeQuery();
            while (getDishesRs.next()) {
                Dish dish = getDishFromResultSet(getDishesRs, connection);
                dishes.add(dish);
            }
            return dishes;
        } catch (SQLException e) {
            throw new DaoException("Exception in DishDaoImpl.getByType()", e);
        }
    }

    @Override
    public Dish getById(long dishId) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement getDishStatement = connection.prepareStatement(GET_BY_ID)){
            getDishStatement.setLong(1, dishId);
            ResultSet getDishRs = getDishStatement.executeQuery();
            Dish dish = null;
            while (getDishRs.next()) {
                dish = getDishFromResultSet(getDishRs, connection);
            }
            return dish;
        } catch (SQLException e) {
            throw new DaoException("Exception in DishDaoImpl.getById(long dishId)", e);
        }
    }

    @Override
    public Set<Dish> getOrderDishes(long orderId, OrderStatus status) throws DaoException {
        Set<Dish> dishes = new HashSet<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection()) {
            PreparedStatement forOrderPs;
            if (status == OrderStatus.DONE) {
                forOrderPs = connection.prepareStatement(GET_FOR_ORDER_ARCHIVE);
                forOrderPs.setLong(1, orderId);
                forOrderPs.setLong(2, orderId);
            } else {
                forOrderPs = connection.prepareStatement(GET_FOR_ORDER);
                forOrderPs.setLong(1, orderId);
            }
            ResultSet forOrderRs = forOrderPs.executeQuery();
            while (forOrderRs.next()) {
                int numberDishes = forOrderRs.getInt("number_dishes");
                Dish dish = getDishFromResultSet(forOrderRs, connection);
                dish.setAmount(numberDishes);
                dishes.add(dish);
            }
            forOrderRs.close();
            forOrderPs.close();
        } catch (SQLException e) {
            throw new DaoException("Exception in DishDaoImpl.getOrderDishes()", e);
        }
        return dishes;
    }

    @Override
    public boolean findInOrders(long dishId) throws DaoException {
        boolean isFound = false;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(FIND_IN_ORDERS)) {
            statement.setLong(1, dishId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                isFound = true;
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in DishDaoImpl.findInOrders()", e);
        }
        return isFound;
    }

    private long insertUpdateDish (PreparedStatement statement, Dish dish) throws SQLException {
        statement.setString(1, dish.getName());
        statement.setString(2, dish.getType().toString());
        statement.setString(3, dish.getDescription());
        statement.setInt(4, dish.getWeight());
        statement.setInt(5, dish.getCost());
        long dishId;
        if (dish.getId() == -1) {
            statement.executeUpdate();

            dishId = -1;
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                dishId = resultSet.getLong(1);
            }
        } else {
            statement.setLong(6, dish.getId());
            statement.executeUpdate();
            dishId = dish.getId();
        }
        return dishId;
    }

    private void insertUpdateDishIngredient (Connection connection, Dish dish) throws SQLException {
        for (String ingredient: dish.getIngredients()) {
            try (PreparedStatement insertIngredStatement = connection.prepareStatement(INSERT_INGREDIENT,
                    PreparedStatement.RETURN_GENERATED_KEYS);
                 PreparedStatement getIdStatement = connection.prepareStatement(GET_INGRED_ID_BY_NAME);
                 PreparedStatement insertDishIngredStatement = connection.prepareStatement(INSERT_DISH_INGREDIENT)) {

                insertIngredStatement.setString(1, ingredient);
                insertIngredStatement.executeUpdate();

                long ingredientId = -1;
                ResultSet insertIngredRs = insertIngredStatement.getGeneratedKeys();
                if (insertIngredRs.next()) {
                    ingredientId = insertIngredRs.getLong(1);
                } else {
                    getIdStatement.setString(1, ingredient);
                    ResultSet getIdRs = getIdStatement.executeQuery();
                    if (getIdRs.next()) {
                        ingredientId = getIdRs.getLong("id");
                    }
                }
                insertDishIngredStatement.setLong(1, dish.getId());
                insertDishIngredStatement.setLong(2, ingredientId);
                insertDishIngredStatement.executeUpdate();
            }
        }
    }

    private Dish getDishFromResultSet (ResultSet getDishesRs, Connection connection) throws SQLException {
        Dish dish;
        long dishId = getDishesRs.getLong("id");
        String name = getDishesRs.getString("name");
        DishType type = DishType.valueOf(getDishesRs.getString("type").toUpperCase());
        String description = getDishesRs.getString("description");
        int weight = getDishesRs.getInt("weight");
        int cost = getDishesRs.getInt("cost");

        PreparedStatement getDishIngredPs = connection.prepareStatement(GET_DISH_INGREDIENTS);
        getDishIngredPs.setLong(1, dishId);
        getDishIngredPs.setLong(2, dishId);
        ResultSet getDishIngredRs = getDishIngredPs.executeQuery();
        ArrayList<String> ingredients = new ArrayList<>();
        while (getDishIngredRs.next()) {
            String ingredient = getDishIngredRs.getString("ingredient");
            ingredients.add(ingredient);
        }
        dish = new Dish(dishId, name, type, description, ingredients, weight, cost);
        getDishIngredRs.close();
        getDishIngredPs.close();
        return dish;
    }

}
