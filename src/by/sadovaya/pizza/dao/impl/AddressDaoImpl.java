package by.sadovaya.pizza.dao.impl;

import by.sadovaya.pizza.dao.AddressDao;
import by.sadovaya.pizza.dao.util.ConnectionUtil;
import by.sadovaya.pizza.entity.Address;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.pool.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Анастасия on 17.02.2016.
 */
public class AddressDaoImpl implements AddressDao {
    private static final Logger LOGGER = Logger.getLogger(AddressDaoImpl.class);
    private static AddressDao instance = new AddressDaoImpl();
    private AddressDaoImpl (){}
    public static AddressDao getInstance() {
        return instance;
    }


    private static final String SELECT_ID_BY_FIELDS = "SELECT id FROM addresses WHERE city=? " +
            "AND street=? AND house=? AND flat=? AND user_id=?";
    private static final String INSERT_ADDRESS = "INSERT INTO addresses (city, street, house, flat, user_id) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String GET_ADDRESSES = "SELECT * FROM addresses WHERE user_id=?";

    private static final String GET_BY_ID = "SELECT * FROM addresses WHERE id=?";

    @Override
    public long save(Address address) throws DaoException {
        long addressId = -1;
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement selectIdPs = connection.prepareStatement(SELECT_ID_BY_FIELDS);
             PreparedStatement insertAddressPs = connection.prepareStatement(INSERT_ADDRESS, Statement.RETURN_GENERATED_KEYS)) {
            connection.setAutoCommit(false);

            setAddressFields(address, selectIdPs);
            ResultSet selectIdRs = selectIdPs.executeQuery();
            while (selectIdRs.next()) {
                addressId = selectIdRs.getLong("id");
            }

            if (addressId == -1) {
                setAddressFields(address, insertAddressPs);
                insertAddressPs.executeUpdate();
                ResultSet resultSet = insertAddressPs.getGeneratedKeys();
                if (resultSet.next()){
                    addressId = resultSet.getLong(1);
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
            return addressId;
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "AddressDaoImpl.save()");
            throw new DaoException("Exception in AddressDaoImpl.save()", e);
        } finally {
            ConnectionUtil.closeConnection(connection, "AddressDaoImpl.save()");
        }
    }

    @Override
    public ArrayList<Address> getByUserId(long userId) throws DaoException {
        ArrayList<Address> addresses = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ADDRESSES)) {
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                addresses.add(getAddressFromResultSet(resultSet));
            }
            return addresses;
        } catch (SQLException e) {
            throw new DaoException("Exception in AddressDaoImpl.getByUserId (long userId)", e);
        }
    }

    @Override
    public Address getById(long id) throws DaoException {
        Address address = null;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_BY_ID)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                address = getAddressFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in AddressDaoImpl.getById(long id)", e);
        }
        return address;
    }

    private void setAddressFields (Address address, PreparedStatement statement) throws SQLException {
        statement.setString(1, address.getCity());
        statement.setString(2, address.getStreet());
        statement.setInt(3, address.getHouse());
        statement.setInt(4, address.getFlat());
        statement.setLong(5, address.getUserId());
    }

    private Address getAddressFromResultSet(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String city = resultSet.getString("city");
        String street = resultSet.getString("street");
        int house = resultSet.getInt("house");
        int flat = resultSet.getInt("flat");
        long userId = resultSet.getLong("user_id");
        Address address = new Address(id, city, street, house, flat, userId);
        return address;
    }

}
