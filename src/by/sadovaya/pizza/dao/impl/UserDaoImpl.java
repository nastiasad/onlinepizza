package by.sadovaya.pizza.dao.impl;

import by.sadovaya.pizza.dao.UserDao;
import by.sadovaya.pizza.dao.util.ConnectionUtil;
import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Анастасия on 07.02.2016.
 */
public class UserDaoImpl implements UserDao {
    private static UserDao instance = new UserDaoImpl();
    private UserDaoImpl (){}
    public static UserDao getInstance (){
        return instance;
    }

    private static final String INSERT_USER = "INSERT INTO users (name, surname, mail, login, password, status) VALUES " +
            "(?, ?, ?, ?, ?, ?)";
    private static final String GET_BY_ID = "SELECT * FROM users WHERE id=?";
    private static final String CHECK_LOGIN_PASSWORD = "SELECT * FROM users WHERE login=? AND password=?";
    private static final String GET_ALL_BY_STATUS = "SELECT * FROM users WHERE status=?";
    private static final String UPDATE_STATUS = "UPDATE users SET status=? WHERE id=?";
    private static final String SET_STATUS = "INSERT INTO courier_status (courier_id, status) VALUES (?, ?)";

    @Override
    public long save(User user) throws DaoException {
        long userId = -1;
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_USER,
                     PreparedStatement.RETURN_GENERATED_KEYS)) {
            connection.setAutoCommit(false);

            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setString(3, user.getMail());
            statement.setString(4, user.getLogin());
            statement.setString(5, user.getPassword());
            statement.setString(6, user.getStatus().toString());
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                userId = resultSet.getLong(1);
            }

            if (user.getStatus() == UserStatus.COURIER) {
                PreparedStatement courierPs = connection.prepareStatement(SET_STATUS);
                courierPs.setLong(1, userId);
                courierPs.setString(2, CourierStatus.FREE.toString());
                courierPs.executeUpdate();
                courierPs.close();
            }

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "UserDaoImpl.save(User user)");
            throw new DaoException("Exception in UserDaoImpl.save(User user)", e);

        } finally {
            ConnectionUtil.closeConnection(connection, "UserDaoImpl.save(User user)");
        }
        return userId;
    }

    @Override
    public User getById(long userId) throws DaoException {
        User user = null;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_BY_ID)) {
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in UserDaoImpl.getById()", e);
        }
        return user;
    }

    @Override
    public User checkLoginPassword(String login, String password) throws DaoException {
        User user = null;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(CHECK_LOGIN_PASSWORD)) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFromResultSet(resultSet);
            }
            resultSet.close();
        } catch (SQLException e) {
            throw new DaoException("Exception in UserDaoImpl.checkLoginPassword()", e);
        }
        return user;
    }

    @Override
    public List<User> getAllCouriers() throws DaoException {
        List<User> couriers = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_ALL_BY_STATUS)) {
            statement.setString(1, UserStatus.COURIER.toString());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                couriers.add(getUserFromResultSet(resultSet));
            }
            return couriers;
        } catch (SQLException e) {
            throw new DaoException("Exception in UserDaoImpl.getAllCouriers()", e);
        }
    }

    @Override
    public void changeStatus(UserStatus status, long userId) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_STATUS)) {
            statement.setString(1, status.toString());
            statement.setLong(2, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in UserDaoImpl.changeStatus()", e);
        }

    }

    private User getUserFromResultSet (ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        String mail = resultSet.getString("mail");
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        UserStatus status = UserStatus.valueOf(resultSet.getString("status"));
        return new User (id, name, surname,mail, login, password, status);
    }
}
