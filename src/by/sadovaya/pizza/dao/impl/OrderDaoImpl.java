package by.sadovaya.pizza.dao.impl;

import by.sadovaya.pizza.dao.OrderDao;
import by.sadovaya.pizza.dao.util.ConnectionUtil;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.entity.Order;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.enums.OrderStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.pool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class OrderDaoImpl implements OrderDao {
    private static OrderDao instance = new OrderDaoImpl();
    private OrderDaoImpl (){}
    public static OrderDao getInstance() {
        return instance;
    }

    private static final String INSERT_ORDER = "INSERT INTO orders (date_time, address_id) " +
            "VALUES (?, ?)";
    private static final String INSERT_ORDER_DISH = "INSERT INTO order_dish (order_id, dish_id, number_dishes) " +
            "VALUES (?, ?, ?)";
    private static final String GET_ALL_NEW = "SELECT * FROM orders WHERE courier_id=-1";
    private static final String GET_FOR_COURIER = "SELECT * FROM orders WHERE courier_id=?";

    private static final String MOVE_TO_ARCHIVE = "INSERT INTO archive_orders SELECT * FROM orders WHERE id=?";
    private static final String MOVE_ORDER_DISH = "INSERT INTO archive_order_dish SELECT * FROM order_dish WHERE order_id=?";

    private static final String DELETE_ORDER = "DELETE FROM orders WHERE id=?";
    private static final String DELETE_ORDER_DISH = "DELETE FROM order_dish WHERE order_id=?";

    private static final String UPDATE_COURIER_STATUS = "UPDATE courier_status SET status=? WHERE courier_id=" +
            "(SELECT courier_id FROM orders WHERE id=?)";

    private static final String GET_FOR_CLIENT = "SELECT o.id AS id, o.date_time AS date_time, o.address_id AS address_id, " +
            "o.courier_id AS courier_id " +
            "FROM orders o " +
            "LEFT JOIN addresses a ON o.address_id = a.id " +
            "LEFT JOIN courier_status cs ON o.courier_id=cs.courier_id " +
            "WHERE a.user_id=?";

    private static final String GET_ARCHIVE_FOR_CLIENT = "SELECT o.id AS id, o.date_time AS date_time, o.address_id AS address_id " +
            "FROM archive_orders o " +
            "LEFT JOIN addresses a ON o.address_id = a.id " +
            "WHERE a.user_id=?";

    public void save(Order order) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement insertOrderPs = connection.prepareStatement(INSERT_ORDER, Statement.RETURN_GENERATED_KEYS)){
            connection.setAutoCommit(false);
            insertOrderPs.setTimestamp(1, new Timestamp(order.getDateTime().getTimeInMillis()));
            insertOrderPs.setLong(2, order.getAddressId());
            insertOrderPs.executeUpdate();

            long orderId = -1;
            ResultSet resultSet = insertOrderPs.getGeneratedKeys();
            if (resultSet.next()){
                orderId = resultSet.getLong(1);
            }
            for (Dish dish: order.getDishes()) {
                try (PreparedStatement insertOrderDishPs = connection.prepareStatement(INSERT_ORDER_DISH)){
                    insertOrderDishPs.setLong(1, orderId);
                    insertOrderDishPs.setLong(2, dish.getId());
                    insertOrderDishPs.setInt(3, dish.getAmount());
                    insertOrderDishPs.executeUpdate();
                }
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "OrderDaoImpl.save()");
            throw new DaoException("Exception in OrderDaoImpl.save()", e);
        } finally {
            ConnectionUtil.closeConnection(connection, "OrderDaoImpl.save()");
        }
    }

    @Override
    public List<Order> getAllNew() throws DaoException {
        List<Order> orders = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(GET_ALL_NEW);
            while (resultSet.next()) {
                Order order = getOrderFromRs(resultSet);
                orders.add(order);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in OrderDaoImpl.getAllNew()", e);
        }
        return orders;
    }

    @Override
    public List<Order> getByCourierId(long courierId) throws DaoException {
        List<Order> orders = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_FOR_COURIER)) {
            statement.setLong(1, courierId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = getOrderFromRs(resultSet);
                orders.add(order);
            }
            return orders;
        } catch (SQLException e) {
            throw new DaoException("Exception in OrderDaoImpl.getByCourierId()", e);
        }
    }

    @Override
    public List<Order> getByClientId(long clientId) throws DaoException {
        List<Order> orders = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement currentPs = connection.prepareStatement(GET_FOR_CLIENT);
             PreparedStatement prevPs = connection.prepareStatement(GET_ARCHIVE_FOR_CLIENT)) {

            currentPs.setLong(1, clientId);
            ResultSet currentRs = currentPs.executeQuery();
            while (currentRs.next()) {
                Order order = getOrderFromRs(currentRs);
                long courierId = currentRs.getLong("courier_id");
                if (courierId == -1) {
                    order.setStatus(OrderStatus.NEW);
                } else {
                    order.setStatus(OrderStatus.ON_THE_WAY);
                }
                orders.add(order);
            }
            prevPs.setLong(1, clientId);
            ResultSet prevRs = prevPs.executeQuery();
            while (prevRs.next()) {
                Order order = getOrderFromRs(prevRs);
                order.setStatus(OrderStatus.DONE);
                orders.add(order);
            }
            return orders;
        } catch (SQLException e) {
            throw new DaoException("Exception in OrderDaoImpl.getByClientId()", e);

        }
    }

    @Override
    public void moveToArchive(long orderId) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement movePs = connection.prepareStatement(MOVE_TO_ARCHIVE);
             PreparedStatement moveOrderDishPs = connection.prepareStatement(MOVE_ORDER_DISH);
             PreparedStatement deleteOrderPs = connection.prepareStatement(DELETE_ORDER);
             PreparedStatement deleteOrderDishPs = connection.prepareStatement(DELETE_ORDER_DISH);
             PreparedStatement updateCourierStatusPs = connection.prepareStatement(UPDATE_COURIER_STATUS)){
            connection.setAutoCommit(false);

            movePs.setLong(1, orderId);
            movePs.executeUpdate();

            moveOrderDishPs.setLong(1, orderId);
            moveOrderDishPs.executeUpdate();

            deleteOrderPs.setLong(1, orderId);
            deleteOrderPs.executeUpdate();

            deleteOrderDishPs.setLong(1, orderId);
            deleteOrderDishPs.executeUpdate();

            updateCourierStatusPs.setString(1, CourierStatus.FREE.toString());
            updateCourierStatusPs.setLong(2, orderId);
            updateCourierStatusPs.executeUpdate();

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "OrderDaoImpl.moveToArchive()");
            throw new DaoException("Exception in OrderDaoImpl.moveToArchive()", e);

        } finally {
            ConnectionUtil.closeConnection(connection, "OrderDaoImpl.moveToArchive()");
        }
    }


    private Order getOrderFromRs (ResultSet resultSet) throws SQLException {
        long orderId = resultSet.getLong("id");
        Calendar dateTime = Calendar.getInstance();
        Timestamp timestamp = resultSet.getTimestamp("date_time");
        dateTime.setTimeInMillis(timestamp.getTime());
        long addressId = resultSet.getLong("address_id");
        return new Order(orderId, dateTime, addressId);
    }
}