package by.sadovaya.pizza.dao.impl;

import by.sadovaya.pizza.dao.CourierDao;
import by.sadovaya.pizza.dao.util.ConnectionUtil;
import by.sadovaya.pizza.enums.CourierStatus;
import by.sadovaya.pizza.exception.DaoException;
import by.sadovaya.pizza.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Анастасия on 18.02.2016.
 */
public class CourierDaoImpl implements CourierDao {
    private static CourierDao instance = new CourierDaoImpl();
    private CourierDaoImpl (){}
    public static CourierDao getInstance() {
        return instance;
    }

    private static final String GET_FREE = "SELECT courier_id FROM courier_status WHERE status=?";

    private static final String SET_COURIER_FOR_ORDER = "UPDATE orders SET courier_id=? WHERE id=?";

    private static final String UPDATE_STATUS = "UPDATE courier_status SET status=? WHERE courier_id=?";

    private static final String DELETE_COURIER_STATUS = "DELETE FROM courier_status WHERE courier_id=?";

    @Override
    public List<Long> getAllFree() throws DaoException {
        List<Long> courierIds = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(GET_FREE)) {
            statement.setString(1, CourierStatus.FREE.toString());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                courierIds.add(resultSet.getLong("courier_id"));
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in CourierDaoImpl.getAllFree()", e);
        }
        return courierIds;
    }

    @Override
    public void updateStatus(long courierId, CourierStatus status) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_STATUS)) {
            statement.setString(1, status.toString());
            statement.setLong(2, courierId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in CourierDaoImpl.updateStatus()", e);
        }
    }

    @Override
    public void setCourierForOrder(long orderId, long courierId, CourierStatus courierStatus) throws DaoException {
        Connection connection = ConnectionPool.getInstance().getConnection();
        try (PreparedStatement setCourierPs = connection.prepareStatement(SET_COURIER_FOR_ORDER);
            PreparedStatement updateStatusPs = connection.prepareStatement(UPDATE_STATUS)) {
            connection.setAutoCommit(false);

            setCourierPs.setLong(1, courierId);
            setCourierPs.setLong(2, orderId);
            setCourierPs.executeUpdate();

            updateStatusPs.setString(1, courierStatus.toString());
            updateStatusPs.setLong(2, courierId);
            updateStatusPs.executeUpdate();

            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            ConnectionUtil.rollbackConnection(connection, "CourierDaoImpl.setCourierForOrder()");
            throw new DaoException("Exception in CourierDaoImpl.setCourierForOrder()", e);
        } finally {
            ConnectionUtil.closeConnection(connection, "CourierDaoImpl.setCourierForOrder()");
        }
    }


    @Override
    public void removeCourierStatus(long courierId) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_COURIER_STATUS)) {
            statement.setLong(1, courierId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in CourierDaoImpl.removeCourierStatus(long courierId)", e);
        }
    }

}
