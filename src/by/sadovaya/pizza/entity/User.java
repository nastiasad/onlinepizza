package by.sadovaya.pizza.entity;

import by.sadovaya.pizza.enums.UserStatus;

import java.io.Serializable;

/**
 * Created by Анастасия on 07.02.2016.
 */
public class User implements Serializable, Cloneable{
    private long id;
    private String name;
    private String surname;
    private String mail;
    private String login;
    private String password;
    private UserStatus status;

    public User(String name, String surname, String mail, String login, String password, UserStatus status) {
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.login = login;
        this.password = password;
        this.status = status;
    }

    public User(long id, String name, String surname, String mail, String login, String password, UserStatus status) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.login = login;
        this.password = password;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }
}
