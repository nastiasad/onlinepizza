package by.sadovaya.pizza.entity;

import by.sadovaya.pizza.enums.OrderStatus;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class Order implements Serializable, Cloneable{
    private long id = -1;
    private Calendar dateTime;
    private OrderStatus status = OrderStatus.NEW;
    private long addressId;
    private Set<Dish> dishes;

    public Order(long id, Calendar dateTime, long addressId, Set<Dish> dishes) {
        this.id = id;
        this.dateTime = dateTime;
        this.addressId = addressId;
        this.dishes = dishes;
    }

    public Order(Calendar dateTime, long addressId, Set<Dish> dishes) {
        this.dateTime = dateTime;
        this.addressId = addressId;
        this.dishes = dishes;
    }

    public Order(long id, Calendar dateTime, long addressId) {
        this.id = id;
        this.dateTime = dateTime;
        this.addressId = addressId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Calendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    public Set<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(Set<Dish> dishes) {
        this.dishes = dishes;
    }
}
