package by.sadovaya.pizza.entity;

import java.io.Serializable;

/**
 * Created by Анастасия on 27.02.2016.
 */
public class CourierOrder implements Serializable, Cloneable{
    private Order order;
    private Address address;
    private String clientName;
    private int total;

    public CourierOrder(Order order, Address address, String clientName) {
        this.order = order;
        this.address = address;
        this.clientName = clientName;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
