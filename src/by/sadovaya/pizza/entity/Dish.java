package by.sadovaya.pizza.entity;

import by.sadovaya.pizza.enums.DishType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Анастасия on 11.02.2016.
 */
public class Dish implements Serializable, Cloneable{
    private long id = -1;
    private String name;
    private DishType type;
    private String description;
    private List<String> ingredients;
    private int weight;
    private int cost;
    private int amount = 1;

    public Dish(String name, DishType type, String description, List<String> ingredients, int weight, int cost) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.ingredients = ingredients;
        this.weight = weight;
        this.cost = cost;
    }

    public Dish(long id, String name, DishType type, String description, List<String> ingredients, int weight, int cost) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
        this.ingredients = ingredients;
        this.weight = weight;
        this.cost = cost;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DishType getType() {
        return type;
    }

    public void setType(DishType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void incAmount () {
        this.amount ++;
    }

    public void decAmount () {
        this.amount --;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dish dish = (Dish) o;

        if (cost != dish.cost) return false;
        if (id != dish.id) return false;
        if (weight != dish.weight) return false;
        if (!description.equals(dish.description)) return false;
        if (!ingredients.equals(dish.ingredients)) return false;
        if (!name.equals(dish.name)) return false;
        if (type != dish.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + ingredients.hashCode();
        result = 31 * result + weight;
        result = 31 * result + cost;
        return result;
    }
}
