package by.sadovaya.pizza.entity;

import java.io.Serializable;

/**
 * Created by Анастасия on 16.02.2016.
 */
public class Address implements Serializable, Cloneable{
    private long id = -1;
    private String city;
    private String street;
    private int house;
    private int flat;
    private long userId;

    public Address(String city, String street, int house, int flat, long userId) {
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.userId = userId;
    }

    public Address(long id, String city, String street, int house, int flat, long userId) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouse() {
        return house;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    public int getFlat() {
        return flat;
    }

    public void setFlat(int flat) {
        this.flat = flat;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return city + ", " + street + " street, " + house + ", " + flat;
    }
}
