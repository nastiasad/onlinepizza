package by.sadovaya.pizza.tag;

import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.enums.DishType;
import by.sadovaya.pizza.enums.UserStatus;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by Анастасия on 23.02.2016.
 */
public class DropdownDishTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(DropdownDishTag.class);

    private String nameParam;
    private String userStatus;

    public void setNameParam (String nameParam) {
        this.nameParam = nameParam;
    }
    public void setUserStatus (String userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public int doStartTag () throws JspException {
        StringBuffer sb = new StringBuffer();

        sb.append("<li class=\"dropdown\">")
                .append("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" ")
                .append("aria-expanded=\"true\">").append(PageContentManager.getProperty(nameParam))
                .append("<span class=\"caret\"></span></a>")
                .append("<ul class=\"dropdown-menu\">");
        for (DishType type: DishType.values()) {
            String strType = type.toString().toLowerCase();
            String rbType = PageContentManager.getProperty("label." + strType);
            sb.append("<li>")
                    .append("<form class=\"wrapper-form\" id=\"").append(strType).append("Form\" method=\"get\" action=\"controller\">")
                    .append("<input type=\"hidden\" name=\"command\" value=\"");

            if (userStatus.equals(UserStatus.ADMIN.toString().toLowerCase())) {
                sb.append(userStatus).append("-");
            }
            sb.append("dishes-redirect\"/>")
                    .append("<input type=\"hidden\" name=\"dishType\" value=\"").append(strType).append("\"/>")
                    .append("<a href=\"#\" onclick=\"document.getElementById('").append(strType).append("Form').submit()\">").append(rbType).append("</a>")
                    .append("</form>")
                    .append("</li>");
        }
        sb.append("</ul>").append("</li>");

        String code = new String (sb);
        try {
            pageContext.getOut().write(code);
        } catch (IOException e) {
            LOGGER.error("Exception in DropdownDishTag", e);
        }
        return SKIP_BODY;
    }

}
