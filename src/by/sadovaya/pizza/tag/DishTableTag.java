package by.sadovaya.pizza.tag;

import by.sadovaya.pizza.command.PageContentManager;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.enums.UserStatus;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Анастасия on 22.02.2016.
 */
@SuppressWarnings("serial")
public class DishTableTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(DishTableTag.class);
    private ArrayList<Dish> dishes;
    private UserStatus userStatus;
    public void setDishes(ArrayList<Dish> dishes) {
        this.dishes = dishes;
    }

    public void setUserStatus (UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public int doStartTag () throws JspException {
        String addToOrder = PageContentManager.getProperty("button.add-to-order");
        String edit = PageContentManager.getProperty("button.edit");
        String remove = PageContentManager.getProperty("button.remove");
        String ingredients = PageContentManager.getProperty("label.ingredients");
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < dishes.size(); i++) {
            if (i % 2 == 0) {
                sb.append("<div class=\"row\">");
            }
            sb.append("<div class=\"col-md-6 dish-cell\">")
                    .append("<h2>").append(dishes.get(i).getName()).append("</h2>")
                    .append("<p>").append(dishes.get(i).getDescription()).append("</p>")
                    .append("<div class=\"dropdown\">")
                    .append("<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">")
                    .append(ingredients)
                    .append("<span class=\"caret\"></span>")
                    .append("</button>")
                    .append("<ul class=\"dropdown-menu dropdown-menu-right custom-dropdown\" aria-labelledby=\"dropdownMenu1\">");

            for (String ingred: dishes.get(i).getIngredients()) {
                sb.append("<li>").append(ingred).append("</li>");
            }
            sb.append("</ul></div>")
                    .append("<h3>").append(dishes.get(i).getWeight()).append(" g</h3>")
                    .append("<h3>").append(dishes.get(i).getCost()).append(" BYR</h3>");

            if (userStatus == UserStatus.CLIENT) {
                sb.append("<form method=\"post\" action=\"controller\">")
                        .append("<input type=\"hidden\" name=\"command\" value=\"client-add-to-order\">")
                        .append("<input type=\"hidden\" name=\"addToOrderId\" value=\"").append(dishes.get(i).getId()).append("\">")
                        .append("<input type=\"submit\" class=\"custom-btn\" value=\"").append(addToOrder).append("\">")
                        .append("</form>");
            } else if (userStatus == UserStatus.ADMIN) {
                sb.append("<form method=\"get\" action=\"controller\">")
                        .append("<input type=\"hidden\" name=\"command\" value=\"admin-edit-dish-redirect\">")
                        .append("<input type=\"hidden\" name=\"dishEditId\" value=\"").append(dishes.get(i).getId()).append("\">")
                        .append("<input type=\"submit\" class=\"custom-btn\" value=\"").append(edit).append("\">")
                        .append("</form>");
                sb.append("<form method=\"post\" action=\"controller\">")
                        .append("<input type=\"hidden\" name=\"command\" value=\"admin-delete-dish\">")
                        .append("<input type=\"hidden\" name=\"deleteDishId\" value=\"").append(dishes.get(i).getId()).append("\">")
                        .append("<input type=\"submit\" class=\"custom-btn\" value=\"").append(remove).append("\">")
                        .append("</form>");
            }
            sb.append("</div>");
            if (i % 2 == 1) {
                sb.append("</div>");
            }
        }
        if (dishes.size() % 2 == 1) {
            sb.append("</div>");
        }
        String code = new String(sb);
        try {
            pageContext.getOut().write(code);
        } catch (IOException e) {
            LOGGER.error("Exception in DishTableTag", e);
        }
        return SKIP_BODY;
    }
}
