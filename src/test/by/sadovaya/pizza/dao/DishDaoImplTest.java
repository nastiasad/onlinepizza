package test.by.sadovaya.pizza.dao;

import by.sadovaya.pizza.dao.impl.DishDaoImpl;
import by.sadovaya.pizza.entity.Dish;
import by.sadovaya.pizza.exception.DaoException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by anastasiya on 13.04.16.
 */
public class DishDaoImplTest {

    private int oldCost = -1;
    private long testedDishId = 11;

    @Test
    public void someTest () throws DaoException {
        Dish dish = DishDaoImpl.getInstance().getById(testedDishId);
        oldCost = dish.getCost();

        int expectedCost = 100_000;
        dish.setCost(expectedCost);
        DishDaoImpl.getInstance().update(dish);
        Dish updatedDish = DishDaoImpl.getInstance().getById(testedDishId);

        Assert.assertEquals(expectedCost, updatedDish.getCost(), 0.01);
    }

    @After
    public void returnPrevValue() throws DaoException {
        if (oldCost >= 0) {
            Dish dish = DishDaoImpl.getInstance().getById(testedDishId);
            dish.setCost(oldCost);
            DishDaoImpl.getInstance().update(dish);
        }
    }
}
