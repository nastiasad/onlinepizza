package test.by.sadovaya.pizza.encryption;

import by.sadovaya.pizza.encryption.Encryption;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by anastasiya on 13.04.16.
 */
public class EncryptionTest {

    @Test
    public void encryptPasswordTest () {
        String actualString = "password";
        String actual = Encryption.encryptPassword(actualString);
        String expected = "5f4dcc3b5aa765d61d8327deb882cf99";
        Assert.assertTrue(actual.equals(expected));
    }
}
