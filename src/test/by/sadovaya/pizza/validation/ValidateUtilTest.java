package test.by.sadovaya.pizza.validation;

import by.sadovaya.pizza.entity.User;
import by.sadovaya.pizza.enums.UserStatus;
import by.sadovaya.pizza.validation.ValidateUtil;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by anastasiya on 13.04.16.
 */
public class ValidateUtilTest {

    @Test
    public void validateUserPositiveTest () {
        User user = new User("nastia", "sad", "sadovaya@gmail.com", "nastiasad", "mypassword", UserStatus.CLIENT);
        Assert.assertTrue(ValidateUtil.validateUser(user));
    }

    @Test
    public void validateUserNegativeTest () {
        User user = new User("nastia", "sad", "sadovaya@gmail", "nastiasad", "mypassword", UserStatus.CLIENT);
        Assert.assertFalse(ValidateUtil.validateUser(user));
    }
}
